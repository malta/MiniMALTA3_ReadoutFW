library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
library UNISIM;
use UNISIM.VComponents.all;

entity ClkPush is
 Port ( 
  i_clk  : in std_logic;
  i_rst  : in std_logic; 
  i_enb  : in std_logic;
  o_sig  : out std_logic
 );
end ClkPush;


architecture Main of ClkPush is
  
 signal m_sig  : std_logic := '0';
 signal m_count: natural  range 0 to 4 :=0;
 signal m_tmp   : std_logic := '0'; 
 signal m_tmp2  : std_logic := '0'; 
  
begin

 o_sig <= m_sig;

 process (i_clk) is 
 begin  
    if rising_edge(i_clk) then
        if i_enb='1' and m_count=0 then
            m_sig <= '1';
            m_count <=1;
        elsif m_count=1 then
            m_count <=2;
            m_sig <='0'; --VD remove it to make it longer ....
        elsif m_count=2 then
            m_count <=3;
            m_sig <='0';
        else 
            if i_rst='1' then
              m_count<=0;
            end if;
        end if;
    end if;
    
 end process;

end Main;