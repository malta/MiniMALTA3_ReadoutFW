----------------------------------------------------------------------------------
-- Engineer: you wish
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity L1AClipper is
generic(
  --all numbers assume a 3.125 ns clk (320MHz):  500 ns + 500ns*step
  i_offset     : integer := 160;
  i_multiplier : integer := 160;
  i_step       : integer :=   0
);
Port ( 
  i_clk    : in STD_LOGIC;
  i_sig    :  in STD_LOGIC;
  o_sig    : out STD_LOGIC;
  o_wind   : out STD_LOGIC
);
end L1AClipper;

architecture Behavioral of L1AClipper is
  --at 320MHz 1 clk cycle is 3.125 ns
  
  --for the moment need to miplement a large window (up to 10musec) and I can put the L1 veto on the same footing
  -- 10000/3.125 = 3200
  -- 50000/3.125 =16000
  
  signal m_window     : integer range 0 to 16383 :=1000; --was 8191
  
  signal m_counter    : integer range 0 to 16383 :=0; 
  signal m_sig        : std_logic := '0';
  signal m_veto       : std_logic := '0';
  signal m_AnotherSig : std_logic := '0';
    
begin
    
  o_sig  <= m_sig;
  o_wind <= m_veto;
  
  m_window <= (i_offset + i_multiplier*i_step); 
    
  process (i_clk) is begin 
    if (rising_edge(i_clk)) then
      m_AnotherSig  <= i_sig; --passing through a flip-flop to improve quality
      
      if m_counter=0 and m_AnotherSig='1' then
        m_counter <= m_counter+1;
        m_sig  <= '1';
        m_veto <= '1';
      elsif m_counter=0 then
        m_sig  <= '0';
        m_veto <= '0';
      elsif m_counter=1 then
        m_sig <= '0';
        m_counter <= m_counter+1;
      elsif m_counter=m_window  then  
        m_veto <= '0';
        m_counter <= 0;
      else
        m_counter <= m_counter+1;
      
      end if; 
    end if; 
  end process;
    
end Behavioral;
