----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 12/05/2020 09:16:08 AM
-- Design Name: 
-- Module Name: MALTA2_SC_Emulator - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
library UNISIM;
use UNISIM.VComponents.all;

entity MiniMALTA3_SCEmulator is 
  Port (        
    i_clk_SCEM       : in STD_LOGIC;
    i_data_SCEM      : in STD_LOGIC;
    i_load_SCEM      : in STD_LOGIC;
    i_rst_SCEM       : in STD_LOGIC;
    o_sout_SCEM      : out STD_LOGIC
  );
end MiniMALTA3_SCEmulator;

architecture Behavioral of MiniMALTA3_SCEmulator is
      
signal m_defWord :      std_logic_vector(435 downto 0) := (others => '0'); --:= x"AAAA_CCCC_BBBB_1111_AAAA_AAAA_AA"; 
signal m_theWord :      std_logic_vector(435 downto 0) := (others => '1');
signal m_default_conf:  std_logic_vector(435 downto 0) := (others => '0');
    
signal m_countR :       natural range 0 to 450 := 0;
signal m_countreset:    natural range 0 to 450 := 0;   
signal m_doRead :       std_logic :='0';
signal m_theOut:        std_logic :='0';                      --Intermediate serial output signal
signal m_doWrite:       std_logic := '0';
signal m_countW :       natural range 0 to 450 := 0;
  
begin

    m_default_conf(0)               <= '1';     		    -- CLKDIV_CTRL_syncBCID_d; 		 
    m_default_conf(5 downto 1)      <= "01000";             -- CLKDIV_CTRL_syncFT_d;    
    m_default_conf(10 downto 6)     <= "00100";             -- CLKDIV_CTRL_prio_d;        
    m_default_conf(15 downto 11)    <= "01000";             -- CLKDIV_CTRL_fastcom_d;        
    m_default_conf(20 downto 16)    <= "01000";             -- FASTCOM_CTRL_pulseWidth_d;       
    m_default_conf(21)              <= '1';                 -- AURORA_clkComp_d;                
    m_default_conf(22)              <= '1';                 -- AURORA_fsp_d;            
    m_default_conf(23)              <= '0';                 -- AURORA_sendSS_d;            
    m_default_conf(24)              <= '0';                 -- AURORA_repeatSS_d;        
    m_default_conf(25)              <= '0';                 -- AURORA_debugEn_d;        
    m_default_conf(26)              <= '0';                 -- AURORA_CTRL_muxO_d;        
    m_default_conf(27)              <= '0';                 -- AURORA_CTRL_muxI_d;        
    m_default_conf(43 downto 28)    <= (others => '1');     -- PERIPHERY_maskD_d;               
    m_default_conf(59 downto 44)    <= (others => '1');     -- PERIPHERY_maskV_d;               
    m_default_conf(75 downto 60)    <= (others => '1');     -- PERIPHERY_maskH_d;            
    m_default_conf(91 downto 76)    <= (others => '0');     -- PERIPHERY_pulseV_d;            
    m_default_conf(107 downto 92)   <= (others => '0');     -- PERIPHERY_pulseH_d;        
    m_default_conf(155 downto 108)  <= (others => '1');     -- MATRIX_maskH_d;            
    m_default_conf(203 downto 156)  <= (others => '0');     -- MATRIX_pulseH_d;                
    m_default_conf(235 downto 204)  <= (others => '0');     -- PERIPHERY_delayCtrl_d;           
    m_default_conf(239 downto 236)  <= (others => '1');     -- SYNC_enFT_d;                    
    m_default_conf(243 downto 240)  <= (others => '1');     -- SYNC_enBC_d;                     
    m_default_conf(248 downto 244)  <= X"1C";               -- LAPA_enCMFB;                     
    m_default_conf(253 downto 249)  <= X"1C";               -- LAPA_enHBRIDGE;                  
    m_default_conf(269 downto 254)  <= X"00FF";             -- LAPA_enPRE;                      
    m_default_conf(270)             <= '1';                 -- LAPA_en;                            
    m_default_conf(274 downto 271)  <= X"7";                -- LAPA_setIBCMFB;                  
    m_default_conf(278 downto 275)  <= X"2";                -- LAPA_setIVNH;                    
    m_default_conf(282 downto 279)  <= X"D";                -- LAPA_setIVNL;                    
    m_default_conf(286 downto 283)  <= X"D";                -- LAPA_setIVPH;                    
    m_default_conf(290 downto 287)  <= X"7";                -- LAPA_setIVPL;                    
    m_default_conf(294 downto 291)  <= X"9";                -- MONITORING_ctrlSFN;              
    m_default_conf(298 downto 295)  <= X"5";                -- MONITORING_ctrlSFP;              
    m_default_conf(308 downto 299)  <= X"A0";               -- DACS_ctrlITHR;                   
    m_default_conf(318 downto 309)  <= X"96";               -- DACS_ctrlIBIAS;                    
    m_default_conf(328 downto 319)  <= X"FF";               -- DACS_ctrlIRESET;                    
    m_default_conf(338 downto 329)  <= X"02";               -- DACS_ctrlICASN;                    
    m_default_conf(348 downto 339)  <= X"96";               -- DACS_ctrlIDB;                    
    m_default_conf(358 downto 349)  <= (others => '0');     -- DACS_ctrlVL;                    
    m_default_conf(368 downto 359)  <= X"FF";               -- DACS_ctrlVH;                    
    m_default_conf(378 downto 369)  <= X"82";               -- DACS_ctrlVRESETD;            
    m_default_conf(388 downto 379)  <= X"FF";               -- DACS_ctrlVCLIP;                    
    m_default_conf(398 downto 389)  <= X"A5";               -- DACS_ctrlVCAS;                    
    m_default_conf(408 downto 399)  <= X"05";               -- DACS_ctrlVREF;                    
    m_default_conf(409)             <= '1';                 -- FASTCOM_CTRL_bypassFC_D;         
    m_default_conf(417 downto 410)  <= (others => '0');     -- FASTCOM_CTRL_RESET_SC_d;            
    m_default_conf(425 downto 418)  <= (others => '0');     -- FASTCOM_CTRL_PULSE_SC_d;            
    m_default_conf(427 downto 426)  <= "11";                -- PLL_CTRL_src_d;                
    m_default_conf(429 downto 428)  <= (others => '0');     -- DEBUG_PRIO_enableSC_d;           
    m_default_conf(435 downto 430)  <= (others => '0');     -- DEBUG_SYNC_SC_readmem_d;
    o_sout_SCEM      <= m_theOut;
   
    --Serial Output
    process(i_clk_SCEM) is
	   begin
	   if rising_edge(i_clk_SCEM) then
	      m_theOut <= m_theWord(m_countW);
		  if m_countW = 4321 then
			 m_countW <= 0; 
		  else
		     m_countW <= m_countW + 1; 
		  end if;
       end if; 
	end process;   

    --Serial Input
	process(i_clk_SCEM) is
	   begin 
	   if rising_edge(i_clk_SCEM) then
		   if i_rst_SCEM = '1' then
			  m_theWord(4321-m_countR) <= i_data_SCEM;
			  if m_countR = 4321 then
				 m_countR <= 0; 
			  else
			     m_countR <= m_countR + 1;
			  end if; 
		   elsif i_load_SCEM = '1' then
			   m_defWord <= m_theWord;  --- This is the value that will be loaded in the corresponding registers
		   elsif i_rst_SCEM = '0' then
		      m_theWord <= m_default_conf;
			  m_countR  <= 0;
		  end if;
	   end if; 
    end process; 
	
end Behavioral; 
	        

