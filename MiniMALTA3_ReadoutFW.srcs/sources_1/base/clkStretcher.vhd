library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
library UNISIM;
use UNISIM.VComponents.all;

entity clkStretcher is
    generic(
        STR_MULTIPLIER      : natural := 10000
    );
    port(
        IN_STR_CLK          :  in std_logic;
        OUT_STR_CLK         : out std_logic
    );
end clkStretcher;

architecture main of clkStretcher is
    -- signals for the multiplier and the slow clock output
    signal s_counter    : natural   range 0 to STR_MULTIPLIER   := 0;
    signal s_slowClk    : std_logic                             := '0';

begin
    process(IN_STR_CLK) is
    begin
    if rising_edge(IN_STR_CLK) then
        if s_counter = STR_MULTIPLIER then
            s_slowCLK <= not s_slowCLK;
            s_counter <= 0;
        else
            s_counter <= s_counter+1;
        end if;
    end if;
    end process;
     
    OUT_STR_CLK <= s_slowCLK;
     
end main;