----------------------------------------------------------------------------------
-- 8 bits are received each clock cycle
-- bits 0-3 are samples of the first bit received by the oversampling ISERDESE modules, 4-7 samples of the second
-- use tap delays to stabilize the 4 samples of both bits
-- how to do this best?

-- *: sample point
-- 
-- CASE 1: 
-- Move the 4 sample points along the bits until they all measure the same value, i.e. they are all within the same bit
-- This might be too much to ask since the 1.2GHz signal is not so stable, maybe go 4/4 and 3/4 instead.
-- Find the tap position which has 3/4 or 4/4 bits equal most of the times out of 16384 samples and move to that position.
-- Once the position is found, only use the middle samples to pipe out data.
-- If the middle two samples do not match too often within a given number of samples, realign.
--
--
--   misaligned sampling points:            *   *   *   * -> 
--   1.2GHz line:   <    bit 00    ><    bit 01    ><    bit 02    ><    bit 03    >
--                                     
--                  
--   aligned sampling points                          *   *   *   * 
--                  <    bit 00    ><    bit 01    ><    bit 02    ><    bit 03    >
--
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
library UNISIM;
use UNISIM.VComponents.all;

entity bitAligner is
port(
    IN_BAL_CLK                  :  in std_logic                     := '0';
    IN_BAL_RESET                :  in std_logic                     := '0';
    IN_BAL_INC                  :  in std_logic                     := '0';
    IN_BAL_EN_INC               :  in std_logic                     := '0';
    IN_BAL_TAP0                 :  in std_logic_vector(4 downto 0)  := (others => '0');
    IN_BAL_TAP1                 :  in std_logic_vector(4 downto 0)  := (others => '0');
    IN_BAL_SAMPLED_BITS         :  in std_logic_vector(7 downto 0)  := (others => '0');
    OUT_BAL_TAP0                : out std_logic_vector(4 downto 0)  := (others => '0');
    OUT_BAL_TAP1                : out std_logic_vector(4 downto 0)  := (others => '0');
    OUT_BAL_LD_TAP              : out std_logic                     := '0';
    OUT_BAL_ALIGNED             : out std_logic                     := '0';
    OUT_BAL_RESET_ACK           : out std_logic                     := '0'
);
end bitAligner;

architecture Align of bitAligner is
    
    -- I/O signals
    signal s_in_clk             : std_logic                         := '0';
    signal s_in_reset           : std_logic                         := '0';
    signal s_in_inc             : std_logic                         := '0';
    signal s_in_en_inc          : std_logic                         := '0';
    signal s_in_tap0            : std_logic_vector(4 downto 0)      := (others => '0');
    signal s_in_tap1            : std_logic_vector(4 downto 0)      := (others => '0');
    signal s_in_sampled_bits    : std_logic_vector(7 downto 0)      := (others => '0');
    signal s_out_tap0           : std_logic_vector(4 downto 0)      := (others => '0');
    signal s_out_tap1           : std_logic_vector(4 downto 0)      := (others => '0');
    signal s_out_ld_tap0        : std_logic                         := '0';
    signal s_out_ld_tap1        : std_logic                         := '0';
    signal s_out_aligned        : std_logic                         := '0';
    signal s_out_reset_ack      : std_logic                         := '0';
    
    -- internal signals
    type match_count is array (0 to 10) of integer range 0 to 16383;
    signal s_match_count        : match_count                       := (others => 0);
    signal i_clk40_count        : integer range 0 to 14             := 0;
    signal i_tap_count          : integer range 0 to 11             := 0;
    signal s_new_tap0           : std_logic_vector(4 downto 0)      := (others => '0');
    signal s_new_tap1           : std_logic_vector(4 downto 0)      := (others => '0');
    
    -- FSM
    signal s_testing            : std_logic                         := '0';
    

begin

    s_in_clk                                <= IN_BAL_CLK;
    s_in_reset                              <= IN_BAL_RESET;
    s_in_inc                                <= IN_BAL_INC;
    s_in_en_inc                             <= IN_BAL_EN_INC;
    s_in_tap0                               <= IN_BAL_TAP0;
    s_in_tap1                               <= IN_BAL_TAP1;
    s_in_sampled_bits                       <= IN_BAL_SAMPLED_BITS;
    

    alignBits: process(s_in_clk) is
    begin
    if rising_edge(s_in_clk) then
        if s_out_reset_ack = '1' then
            if i_clk40_count = 14 then
                s_out_reset_ack             <= '0';
                i_clk40_count               <= 0;
            else
                i_clk40_count               <= i_clk40_count + 1;
            end if;
        end if;
        if s_in_reset = '1' then
            s_out_reset_ack                 <= '1';
        elsif s_out_aligned = '0' then
            -- 1) use current tap (start: 0) and check bits 16384 times
            -- 2) compare match count to current best count and update delay setting and current best count if necessary
            -- 3) increase tap by one, if tap was increased 10 times go to 4, else go to 1
            -- 4) apply tap of best position and set aligned = '1'
            -- -) if reset is raised redo alignment
            
            
            
        end if;
    end if;
    end process alignBits;


end Align;
