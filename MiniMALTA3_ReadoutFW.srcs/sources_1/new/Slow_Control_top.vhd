----------------------------------------------------------------------------------
-- MiniMALTA3 Slow Control
-- Leyre Flores
-- Ignacio Asensi
-- Description: Shift-register implemenation in which you need to send the full 436 
--              bits and then assert the load. All the bits are written to a FIFO
--              and read from a FIFO of 256 bits. 
----------------------------------------------------------------------------------



library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
library UNISIM;
use UNISIM.VComponents.all;
library unimacro;
use unimacro.vcomponents.all;
use work.ipbus.all;
use work.readout_constants.all;
use work.package_constants.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Slow_Control_top is
    Port (
        signal o_REFPULSE_ValidIn  : out    std_logic;
        signal o_SC_loadout        : out    std_logic; 
        signal o_SCclk_enable   : out    std_logic;
        signal i_clk_sc         : in     std_logic;                                       -- Connect a 40MHz clock to generate the rest of the clocks
        signal i_sout           : in     std_logic;                                       -- Values of all registers as a serial stream from MiniMALTA3
        signal o_rstB           : out    std_logic;                                       -- If '0' no input data is taken by MiniMALTA3 and the config of the chip is set to default
        signal o_data_toMalta   : out    std_logic; 
    
        -- control
        signal i_IPBeset        : in     std_logic;
    
        -- ipbus signals
        signal i_clk_ipb        : in     std_logic;                                       -- Connect a 32MHz clock
        signal i_ipbw           : in     ipb_wbus_array(99 downto 0);
        signal i_ipbr           : inout  ipb_rbus_array(99 downto 0);
      --  signal o_LPC_LEDS       : out    std_logic_vector(9 downto 0);
        signal o_LPC_SMA        : out    std_logic_vector(9 downto 0)  
     );
end Slow_Control_top;

architecture Behavioral of Slow_Control_top is
------------------------------------------------
-----    MiniMALTA SHIFT REGISTER BITS    ------
------------------------------------------------

----     CLOCK SELECTION REGISTERS    ----
signal CLKDIV_CTRL_syncBCID_d	 :std_logic := '1';			   		                      -- Synchronization memory BCDI clock switch phase bit 
signal CLKDIV_CTRL_syncFT_d	     :std_logic_vector (4 downto 0) := "01000";               -- Synchronization memory Fast Time Clock Select bits [3:0] from 1.28, GHz 640 MHz, 320 MHz or 160 MHz in one hot encoded and bit [5] is the phase of the Fast Time clock  
signal CLKDIV_CTRL_prio_d 	     :std_logic_vector (4 downto 0) := "00100";               -- Synchronization memory Priority Encoder Clock Select bits [3:0] from 320 MHz, 160 MHz, 80 MHz or 40 MHz in one hot encoded and bit [5] is the phase of the Priority encoeder clock
signal CLKDIV_CTRL_fastcom_d	 :std_logic_vector (4 downto 0) := "01000";               -- Fast Command Encoder Clock Select bits [3:0] from 320 MHz, 160 MHz, 80 MHz or 40 MHz in one hot encoded and bit [5] is the phase of the Fast Command Encoeder clock
signal FASTCOM_CTRL_pulseWidth_d :std_logic_vector (4 downto 0) := "01000";               -- Width of the reset and pulse signals sent by the Fast Command Encoder Block. You can choose from 1 clk cycle to 23 in order to avoid overlap. 
 
----     AURORA CONTROL SIGNALS     ----
signal AURORA_clkComp_d 	     :std_logic := '1'; 					                  -- Aurora Clock Compensation to be always left on except for debugging
signal AURORA_fsp_d		         :std_logic := '1';					                      -- Aurora Frame Separator sends known data frame when there is no data to be read in the FIFO
signal AURORA_sendSS_d		     :std_logic := '0'; 					                  -- Aurora sends a known start sequence before going into the IDLE state 
signal AURORA_repeatSS_d	     :std_logic := '0';					                      -- Aurora remains in the start sequence state and always sends the specified start sequence frame (debugging option) 
signal AURORA_debugEn_d		     :std_logic := '0'; 					                  -- Flag to debug the Aurora block
signal AURORA_CTRL_muxO_d	     :std_logic := '0'; 
signal AURORA_CTRL_muxI_d	     :std_logic := '0'; 

----    MASKING     ----
signal PERIPHERY_maskD_d	     :std_logic_vector (15 downto 0) := (others => '1');      -- Diagonal coordinate for the masking
signal PERIPHERY_maskV_d	     :std_logic_vector (15 downto 0) := (others => '1'); 	  -- Column coordinate of the masking
signal PERIPHERY_maskH_d	     :std_logic_vector (15 downto 0) := (others => '1'); 	  -- Row coordinate of the masking
signal PERIPHERY_pulseV_d	     :std_logic_vector (15 downto 0) := (others => '0');  	  -- Column coordinate for pulsing
signal PERIPHERY_pulseH_d	     :std_logic_vector (15 downto 0) := (others => '0');  	  -- Row coordinate for pulsing
signal MATRIX_maskH_d		     :std_logic_vector (47 downto 0) := (others => '1'); 	  -- Row coordinate of the masking
signal MATRIX_pulseH_d		     :std_logic_vector (47 downto 0) := (others => '0'); 	  -- Row coordinate of the pulsing

----    REFERENCE PULSE WIDTH CONTROL     ----
signal PERIPHERY_delayCtrl_d	 :std_logic_vector (31 downto 0) := (others => '0'); 	  -- Reference pulse width control 

---- SYNCHRONIZATION MEMORY CLOCKS CONTROL ----
signal SYNC_enFT_d		         :std_logic_vector (3 downto 0)  := (others => '1'); 	  -- Enable the Fast Counter clock for each of the 4 synchronization memories individually  
signal SYNC_enBC_d		         :std_logic_vector (3 downto 0)  := (others => '1'); 	  -- Enable the BCID Counter for each of the 4 synchronization memories individually  

----    LAPA CONFIGURATION REGISTERS     ----
signal LAPA_enCMFB		         :std_logic_vector (4 downto 0)  := "11100"; 		          -- Connects the VPL of the respective H-bridge to the feedback reference
signal LAPA_enHBRIDGE		     :std_logic_vector (4 downto 0)  := "11100"; 		          -- Control of the current of the LAPA driver
signal LAPA_enPRE		         :std_logic_vector (15 downto 0) := X"00FF"; 		      -- Pre-emphasis settings of the LAPA driver common to all the outputs/inputs
signal LAPA_en			         :std_logic			             := '1'; 		          -- Enable the lapa driver
signal LAPA_setIBCMFB            :std_logic_vector (3 downto 0)  := X"7";		          -- Bias current of VCM feedback amp
signal LAPA_setIVNH		         :std_logic_vector (3 downto 0)  := X"2";		          -- Ion Hbridge PMOS 
signal LAPA_setIVNL		         :std_logic_vector (3 downto 0)  := X"D";		          -- Ioff Hbridge PMOS 
signal LAPA_setIVPH		         :std_logic_vector (3 downto 0)  := X"D";		          -- Ion Hbridge NMOS 
signal LAPA_setIVPL		         :std_logic_vector (3 downto 0)  := X"7";		          -- Ioff Hbridge NMOS

----    MONITORING PIXELS BIAS CONFIGURATION    ----
signal MONITORING_ctrlSFN	     :std_logic_vector (3 downto 0)  := X"9";		          -- Bias settings of the monitoring pixels type N 
signal MONITORING_ctrlSFP	     :std_logic_vector (3 downto 0)  := X"5";		          -- Bias settings of the monitoring pixels type P

----    DACS CONFIGURATION    ----
signal DACS_ctrlITHR		     :std_logic_vector (9 downto 0)	:= "0010100000";		  -- Ithreshold bias setting should be thermometer encoded   
signal DACS_ctrlIBIAS		     :std_logic_vector (9 downto 0)	:= "0010010110";		          -- Ibias bias setting should be thermometer encoded   
signal DACS_ctrlIRESET		     :std_logic_vector (9 downto 0)	:= "0011111111";		          -- Ireset bias setting should be thermometer encoded   
signal DACS_ctrlICASN		     :std_logic_vector (9 downto 0)	:= "0000000010";		          -- Icasn bias setting should be thermometer encoded   
signal DACS_ctrlIDB		         :std_logic_vector (9 downto 0)	:= "0010010110";		          -- Idb bias setting should be thermometer encoded   
signal DACS_ctrlVL		         :std_logic_vector (9 downto 0)	:= (others => '0');	      -- Vlow bias setting should be one-hot encoded   
signal DACS_ctrlVH		         :std_logic_vector (9 downto 0)	:= "0011111111";		          -- Vhigh bias setting should be one-hot encoded   
signal DACS_ctrlVRESETD		     :std_logic_vector (9 downto 0)	:= "0010000010";		          -- VresetD bias setting should be one-hot encoded   
signal DACS_ctrlVCLIP		     :std_logic_vector (9 downto 0)	:= "0011111111";		          -- Vclip bias setting should be one-hot encoded   
signal DACS_ctrlVCAS		     :std_logic_vector (9 downto 0)	:= "0010100101";		          -- Vcas bias setting should be one-hot encoded   
signal DACS_ctrlVREF		     :std_logic_vector (9 downto 0)	:= "0000000101";		          -- Vref bias setting should be one-hot encoded   

----    FAST COMMAND ENCODER and PRIOITY ENCODER DEBUG SIGNALS     ----
signal FASTCOM_CTRL_bypassFC_D	 :std_logic := '1';					                      -- Chooses between slow control and fast command for sending resets and pulses 
signal FASTCOM_CTRL_RESET_SC_d	 :std_logic_vector (7 downto 0)	:= (others => '0');	      -- Resets to the different logic blocks 
signal FASTCOM_CTRL_PULSE_SC_d	 :std_logic_vector (7 downto 0)	:= (others => '0');	      -- Pulses to the different columns in MiniMALTA3
signal PLL_CTRL_src_d		     :std_logic_vector (1 downto 0) := "11";		          -- MSB=CLK_SRC, LSB = RESETB. When we want to work with the PLL frequency, we need to have RESETB and CLK_SRC signals in ‘1’. 
signal DEBUG_PRIO_enableSC_d     :std_logic_vector (1 downto 0) := (others => '0');	      -- Enables the use of the debug module or disables it
signal DEBUG_SYNC_SC_readmem_d   :std_logic_vector (5 downto 0) := (others => '0');	


---     Debug IPBUS register that tells you the IPAddress of the FPGA    ---
signal m_control		         :std_logic_vector (31 downto 0):= (others => '0');
signal m_reset                   :std_logic_vector (31 downto 0):= (others => '0'); 

---    Complete FIFO that is written to IPBUS    ---
signal m_WWORD_full	             :std_logic_vector(511 downto 0):= (others => '0'); 
signal m_WWORD                   :std_logic_vector(31 downto 0):= (others => '1');      -- Signal that contains the data from IPBus and it is output of the FIFO
signal m_fifoW_WWORD_reg         :std_logic_vector(31 downto 0) := (others => '0');
---    Signals for the WRITE FIFO    ---
signal m_fifoW_reset 		     :std_logic                     := '0'; 
signal m_fifoW_wren 		     :std_logic                     := '0'; 
signal m_fifoW_rden 		     :std_logic                     := '0'; 
signal m_fifoW_word_reg		     :std_logic_vector(31 downto 0) := (others => '0'); 
signal m_fifoW_rdclk             :std_logic                     := '0';
signal m_fifoW_WWORD_dummy       :std_logic_vector(31 downto 0) := (others => '0');
signal m_fifoW_empty             :std_logic                     := '0';
signal m_fifoW_full              :std_logic                     := '1';
signal m_fifoW_almostFull        :std_logic                     := '1';

---    Signals for the slowcontrol ports    ---
signal m_serializer_toMalta     :std_logic                      := '0';
signal m_serializer_fromMalta   :std_logic                      := '0';
signal m_sout_ack               :std_logic                      := '0';

signal m_start                  :std_logic                      := '0';
signal m_WR_start               :std_logic                      := '0';
signal m_RE_start               :std_logic                      := '0';
signal m_ReadEn                 :std_logic                      :='0';
signal m_ReadRST                :std_logic                      :='0';
signal m_WriteEn                :std_logic                      :='0';
signal m_WriteRST               :std_logic                      :='1';
signal m_SC_state               :std_logic                      :='0';
signal m_SC_stateL              :std_logic                      :='0';
signal m_des_en                 :std_logic                      :='0';
signal m_rstB                   :std_logic                      :='1';
signal m_des_busy               :std_logic                      :='0';

---    MiniMALTA3 word    ---
signal m_SC_data_toMalta        :std_logic_vector(435 downto 0) := (others => '0');


---    Control signals    ---
signal im_writing               :std_logic                      :='0';
signal im_reading               :std_logic                      :='0';

---    Counters to keep track of how many bits have been written and read    ---
signal m_writing_counter        :natural range 0 to 879         := 0; -- To be updated 
signal m_R_start                :std_logic                      := '0';
signal m_write_to_ipbus_done    :std_logic                      := '0';


---    Abhi's LED and SMA board for debugging nad sending data to the scope    ----
signal m_LPC_LEDS_SC            :std_logic_vector(9 downto 0)   := (others => '0');
signal m_i_sout_to_SMA_direct   :std_logic;
signal m_i_sout_to_SMA_inclkprocess :std_logic; 
signal m_SCclk_enable           :std_logic := '0';
signal m_SC_load           :std_logic;

---     Definitions of FIFOs     ---
 
---    WRITE data to chip FIFO    --- 

component fifo_user_toSC256 is
    port (
        rst           : IN STD_LOGIC;
        wr_clk        : IN STD_LOGIC;
        rd_clk        : IN STD_LOGIC;
        din           : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
        wr_en         : IN STD_LOGIC;
        rd_en         : IN STD_LOGIC;
        dout          : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
        full          : OUT STD_LOGIC;
        almost_full   : OUT STD_LOGIC;
        empty         : OUT STD_LOGIC

    );
end component fifo_user_toSC256; 

begin
 -- SERIALIZER
portMap_serializer : entity work.serializer
port map(
  i_clock        => i_clk_sc,
  i_dataParallel => m_SC_data_toMalta,
  i_startW       => m_WR_start,   
  o_dataSerial   => m_serializer_toMalta,
  o_serializer_send  => m_SCclk_enable, -- it is high when sending serialized data
  o_ser_load     => m_SC_load 
);



---    IPBUS Register: SlowControl FIFO_WRITE data from user to chip    ---
  
slowcontrol_w: entity work.ipbus_fifo
    generic map(
        c_ok_in_msb => '0'
    )
    port map(
        c_address   => std_logic_vector(to_unsigned(IPBADDR_FIFO,32)), 
        i_clk       => i_clk_IPb,
        i_ipbw      => i_ipbw(IPBADDR_FIFO),
        o_ipbr      => i_ipbr(IPBADDR_FIFO),
        c_readonly  => '0',
        i_empty     => m_fifoW_empty,
        i_full      => m_fifoW_full, 
        i_rddata    => m_fifoW_WWORD_dummy,
        --o_rdclk    => m_fifoR_rdclk,
        o_rdenable  => open,
        o_wrdata    => m_fifoW_WWORD_reg,
        o_wrclk     => open,
        o_wrenable  => m_fifoW_wren 
    ); 

---     Write fifo port mapping     ---
 
 FIFOR_W_WORD : fifo_user_toSC256
     port map(
         rst           => m_fifoW_reset,
         wr_clk        => i_clk_IPb,-- DO NOT CHANGE THIS EVER
         rd_clk        => i_clk_sc,-- DO NOT CHANGE THIS EVER
         din           => m_fifoW_WWORD_reg,
         wr_en         => m_fifoW_wren,
         rd_en         => m_fifoW_rden,
         dout          => m_WWORD,
         full          => m_fifoW_full,
         almost_full   => m_fifoW_almostFull,
         empty         => m_fifoW_empty
     );
    

 --- IPBUS Register: Control register to check IPBUS communication ---
reg_control: entity work.ipbus_register
port map(
   c_address  => std_logic_vector(to_unsigned(IPBADDR_CONTROL,32)),
   i_ipbw     => i_ipbw(IPBADDR_CONTROL),
   o_ipbr     => i_ipbr(IPBADDR_CONTROL),
   i_clk      => i_clk_IPb,  
   i_reset    => i_IPBeset,
   c_readonly => '0',
   c_writeMon => '1',
   i_default  => X"00000000",
   o_signal   => m_control
 );

 m_SC_state  <= m_control(0); 
 o_REFPULSE_ValidIn <=      m_control(1); 
 
  --- IPBUS Register: Control register to check IPBUS communication ---
reg_SC_rst: entity work.ipbus_register
port map(
   c_address  => std_logic_vector(to_unsigned(IPBADDR_SC_RESET,32)),
   i_ipbw     => i_ipbw(IPBADDR_SC_RESET),
   o_ipbr     => i_ipbr(IPBADDR_SC_RESET),
   i_clk      => i_clk_IPb,  
   i_reset    => i_IPBeset,
   c_readonly => '0',
   c_writeMon => '1',
   i_default  => X"00000000",
   o_signal   => m_reset
 );

 m_rstB  <= m_reset(0);    
 
 
 
 

--- Debugging signals to SMA PCB attached to Kintex

--o_LPC_SMA(0)     <= m_SCclk_enable;
--o_LPC_SMA(1)     <= m_rstB;     --m_i_sout_to_SMA_inclkprocess;
--o_LPC_SMA(2)     <= m_serializer_toMalta; 
--o_LPC_SMA(3)     <= i_clk_sc;               -- im_writing;
--o_LPC_SMA(4)     <= m_serializer_toMalta;   -- This is the serial input from User to chip
--o_LPC_SMA(5)     <= m_SC_load;     -- This is the reset 
--o_LPC_SMA(6)     <= m_WR_start;
----o_LPC_SMA(7)     <= im_writing;             --m_fifoR_wren;
----o_LPC_SMA(9)     <= i_sout;-- m_i_sout_to_SMA_inclkprocess;               --This is the SC clock


--o_LPC_LEDS(0)<= m_SC_state;
--o_LPC_LEDS(1)<= '0';
--o_LPC_LEDS(2)<= m_serializer_toMalta;
--o_LPC_LEDS(3)<= '1';
--Output mapping of the signals
o_data_toMalta <=  m_serializer_toMalta;
o_rstB         <=  m_rstB; 

o_SC_loadout       <= m_SC_load;
o_SCclk_enable <=m_SCclk_enable;

--- Architecture of the block ---

--- State machine for action control
--process(i_clk_sc) is
--  begin    
--  if rising_edge(i_clk_sc) then
--    m_SC_stateL <= m_SC_state;
--  end if;
--end process;  

process(i_clk_sc) is
    begin
    if rising_edge(i_clk_sc) then
        m_i_sout_to_SMA_inclkprocess <= i_sout; 
        if (m_SC_state = '0') then  -- resetting
            m_LPC_LEDS_SC          <= "1000000001";
            m_writing_counter      <= 0;
            m_WR_start             <='0';
            m_write_to_ipbus_done  <='0';
            m_WWORD_full           <= (others => '1');

        else                                      		                                    -- STATE SET FROM SW
            if (m_writing_counter < 0 + CC_WRITE_FIFO ) then                     	        -- N CLOCK CYCLES TO WRITE FIFO (CONSTANT)
                m_writing_counter <= m_writing_counter+1;
                -- Filling write fifo
                case m_writing_counter is
                    when 1 =>
                    when 2 => -- YOU HAVE TO WAIT UNTIL 2nd CC!!!
                        PERIPHERY_pulseH_d  <= m_WWORD (  15 downTo 0);
                        LAPA_enPRE          <= m_WWORD (  31 downTo 16);
                    when 3 =>
                        PERIPHERY_maskH_d  <= m_WWORD (  15 downTo 0);
                        PERIPHERY_pulseV_d <= m_WWORD (  31 downTo 16);
                    when 4 => 
                        PERIPHERY_maskD_d   <= m_WWORD (  15 downTo 0);
                        PERIPHERY_maskV_d   <= m_WWORD (  31 downTo 16);
                    when 5 =>
                        DACS_ctrlVCAS   <= m_WWORD (  9 downTo 0);
                        DACS_ctrlVL     <= m_WWORD (  19 downTo 10);
                        DACS_ctrlVH     <= m_WWORD (  29 downTo 20);
                    when 6 => 
                        DACS_ctrlVRESETD   <= m_WWORD (  9 downTo 0);
                        DACS_ctrlVCLIP   <= m_WWORD (  19 downTo 10);
                        DACS_ctrlVREF   <= m_WWORD (  29 downTo 20);
                        AURORA_clkComp_d   <= m_WWORD (30);
                    when 7 =>
                        DACS_ctrlITHR   <= m_WWORD (  9 downTo 0);
                        DACS_ctrlIBIAS   <= m_WWORD (  19 downTo 10);
                        DACS_ctrlICASN   <= m_WWORD (  29 downTo 20);
                        FASTCOM_CTRL_bypassFC_D   <= m_WWORD (30);
                    when 8 => 
                        DACS_ctrlIDB   <= m_WWORD (  9 downTo 0);
                        DACS_ctrlIRESET   <= m_WWORD (  19 downTo 10);
                        FASTCOM_CTRL_PULSE_SC_d   <= m_WWORD (  27 downTo 20);
                    when 9 =>
                        FASTCOM_CTRL_pulseWidth_d <= m_WWORD ( 4 downTo 0);
                        LAPA_enCMFB              <=  m_WWORD (9 downTo 5);
                        LAPA_enHBRIDGE           <=  m_WWORD (14 downTo 10);
                        DEBUG_SYNC_SC_readmem_d  <=  m_WWORD (20 downTo 15);
                        FASTCOM_CTRL_RESET_SC_d  <=  m_WWORD (28 downTo 21);
                        CLKDIV_CTRL_syncBCID_d  <=  m_WWORD (29);
                    when 10 => 
                        LAPA_setIVNH            <=  m_WWORD (3 downTo 0);
                        LAPA_setIVNL            <=  m_WWORD (7 downTo 4);
                        LAPA_setIVPH            <=  m_WWORD (11 downTo 8);
                        LAPA_setIVPL            <=  m_WWORD (15 downTo 12);
                        CLKDIV_CTRL_syncFT_d    <=  m_WWORD (20 downTo 16);
                        CLKDIV_CTRL_prio_d      <=  m_WWORD (25 downTo 21);
                        CLKDIV_CTRL_fastcom_d   <=  m_WWORD (30 downTo 26);
                    when 11 =>
                        AURORA_fsp_d            <=   m_WWORD (0);
                        AURORA_sendSS_d         <=  m_WWORD (1);
                        AURORA_repeatSS_d       <=  m_WWORD (2);
                        AURORA_debugEn_d        <=  m_WWORD (3);
                        AURORA_CTRL_muxO_d      <=  m_WWORD (4);
                        AURORA_CTRL_muxI_d      <=  m_WWORD (5);
                        LAPA_en                 <=  m_WWORD (6);
                        PLL_CTRL_src_d          <=  m_WWORD ( 8 downTo 7);
                        DEBUG_PRIO_enableSC_d   <=  m_WWORD ( 10 downTo 9);
                        MONITORING_ctrlSFN      <=  m_WWORD ( 14  downTo 11);
                        MONITORING_ctrlSFP      <=  m_WWORD (18 downTo 15);
                        SYNC_enFT_d             <=  m_WWORD ( 22 downTo 19);
                        SYNC_enBC_d             <=  m_WWORD ( 26 downTo 23);
                        LAPA_setIBCMFB          <=  m_WWORD (30 downTo 27);
                    when 12 => 
                        PERIPHERY_delayCtrl_d   <= m_WWORD;
                    when 13 =>
                        MATRIX_maskH_d (31 downTo 0)	    <=  m_WWORD;
                    when 14 => 
                        MATRIX_maskH_d (47 downTo 32)	    <=  m_WWORD (  15 downTo 0);        
                    when 15 =>
                        MATRIX_pulseH_d (31 downTo 0)	    <=  m_WWORD;   
                    when 16 => 
                        MATRIX_pulseH_d (47 downTo 32)	    <=  m_WWORD (  15 downTo 0);       
                    when 17 =>
                    when 0  => 
                        m_fifoW_rden   <='1';
                    when others =>
                        m_fifoW_rden   <='0';
                end case;
            elsif (m_writing_counter = CC_WRITE_FIFO ) then
                m_fifoW_reset   		        <='1';
                m_fifoW_rden                    <='0';  -- stop reading write fifo
                im_reading                      <='0';
                m_LPC_LEDS_SC                   <= "0000000000";
                m_WR_start                      <='0';
                m_write_to_ipbus_done           <='0';
                
                m_SC_data_toMalta(0)	<=	CLKDIV_CTRL_syncBCID_d	;
                m_SC_data_toMalta(5 downto 1)    <=    CLKDIV_CTRL_syncFT_d    ;
                m_SC_data_toMalta(10 downto 6)    <=    CLKDIV_CTRL_prio_d    ;
                m_SC_data_toMalta(15 downto 11)    <=    CLKDIV_CTRL_fastcom_d    ;
                m_SC_data_toMalta(20 downto 16)    <=    FASTCOM_CTRL_pulseWidth_d    ;
                m_SC_data_toMalta(21)    <=    AURORA_clkComp_d    ;
                m_SC_data_toMalta(22)    <=    AURORA_fsp_d    ;
                m_SC_data_toMalta(23)    <=    AURORA_sendSS_d    ;
                m_SC_data_toMalta(24)    <=    AURORA_repeatSS_d    ;
                m_SC_data_toMalta(25)    <=    AURORA_debugEn_d    ;
                m_SC_data_toMalta(26)    <=    AURORA_CTRL_muxO_d    ;
                m_SC_data_toMalta(27)    <=    AURORA_CTRL_muxI_d    ;
                m_SC_data_toMalta(43 downto 28)    <=    PERIPHERY_maskD_d    ;
                m_SC_data_toMalta(59 downto 44)    <=    PERIPHERY_maskV_d    ;
                m_SC_data_toMalta(75 downto 60)    <=    PERIPHERY_maskH_d    ;
                m_SC_data_toMalta(91 downto 76)    <=    PERIPHERY_pulseV_d    ;
                m_SC_data_toMalta(107 downto 92)    <=    PERIPHERY_pulseH_d    ;
                m_SC_data_toMalta(155 downto 108)    <=    MATRIX_maskH_d    ;
                m_SC_data_toMalta(203 downto 156)    <=    MATRIX_pulseH_d    ;
                m_SC_data_toMalta(235 downto 204)    <=    PERIPHERY_delayCtrl_d    ;
                m_SC_data_toMalta(239 downto 236)    <=    SYNC_enFT_d    ;
                m_SC_data_toMalta(243 downto 240)    <=    SYNC_enBC_d    ;
                m_SC_data_toMalta(248 downto 244)    <=    LAPA_enCMFB    ;
                m_SC_data_toMalta(253 downto 249)    <=    LAPA_enHBRIDGE    ;
                m_SC_data_toMalta(269 downto 254)    <=    LAPA_enPRE    ;
                m_SC_data_toMalta(270)              <=    LAPA_en    ;
                m_SC_data_toMalta(274 downto 271)    <=    LAPA_setIBCMFB    ;
                m_SC_data_toMalta(278 downto 275)    <=    LAPA_setIVNH    ;
                m_SC_data_toMalta(282 downto 279)    <=    LAPA_setIVNL    ;
                m_SC_data_toMalta(286 downto 283)    <=    LAPA_setIVPH    ;
                m_SC_data_toMalta(290 downto 287)    <=    LAPA_setIVPL    ;
                m_SC_data_toMalta(294 downto 291)    <=    MONITORING_ctrlSFN    ;
                m_SC_data_toMalta(298 downto 295)    <=    MONITORING_ctrlSFP    ;
                m_SC_data_toMalta(308 downto 299)    <=    DACS_ctrlITHR    ;
                m_SC_data_toMalta(318 downto 309)    <=    DACS_ctrlIBIAS    ;
                m_SC_data_toMalta(328 downto 319)    <=    DACS_ctrlIRESET    ;
                m_SC_data_toMalta(338 downto 329)    <=    DACS_ctrlICASN    ;
                m_SC_data_toMalta(348 downto 339)    <=    DACS_ctrlIDB    ;
                m_SC_data_toMalta(358 downto 349)    <=    DACS_ctrlVL    ;
                m_SC_data_toMalta(368 downto 359)    <=    DACS_ctrlVH    ;
                m_SC_data_toMalta(378 downto 369)    <=    DACS_ctrlVRESETD    ;
                m_SC_data_toMalta(388 downto 379)    <=    DACS_ctrlVCLIP    ;
                m_SC_data_toMalta(398 downto 389)    <=    DACS_ctrlVCAS    ;
                m_SC_data_toMalta(408 downto 399)    <=    DACS_ctrlVREF    ;
                m_SC_data_toMalta(409)               <=    FASTCOM_CTRL_bypassFC_D    ;
                m_SC_data_toMalta(417 downto 410)    <=    FASTCOM_CTRL_RESET_SC_d    ;
                m_SC_data_toMalta(425 downto 418)    <=    FASTCOM_CTRL_PULSE_SC_d    ;
                m_SC_data_toMalta(427 downto 426)    <=    PLL_CTRL_src_d    ;
                m_SC_data_toMalta(429 downto 428)    <=    DEBUG_PRIO_enableSC_d    ;
                m_SC_data_toMalta(435 downto 430)    <=    DEBUG_SYNC_SC_readmem_d    ;
                m_writing_counter               <= m_writing_counter+1;

            elsif (m_writing_counter= 1 + CC_WRITE_FIFO ) then        -- send start write command (should take 1 cc)
                m_WR_start                      <='1';
                m_writing_counter               <= m_writing_counter+1;

            elsif ( m_writing_counter < 438 + CC_WRITE_FIFO ) then  -- (LF < 4326) writting time 4324+3
                m_WR_start                      <='0';
                im_writing                      <='1';
                m_writing_counter               <= m_writing_counter+1;

            elsif ( m_writing_counter = 438 + CC_WRITE_FIFO ) then      --(LF = 4326)
                im_writing                      <='0';
                m_R_start                       <='1';-- wait for clock falling edge
                im_reading                      <='0';
                m_writing_counter               <= m_writing_counter+1;
                m_fifoW_reset                   <= '0';

            end if;           
        end if;
    end if;
end process; 


             


end Behavioral;
