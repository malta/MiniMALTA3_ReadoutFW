----------------------------------------------------------------------------------

----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity resetHandler is
generic(
    SIZE                            : natural                                   := 20
);
port(
    IN_CLK                          : in  std_logic                             := '0';
    IN_RESET                        : in  std_logic_vector(SIZE-1 downto 0)     := (others => '0');
    IN_RESET_GLOBAL                 : in  std_logic                             := '0';
    OUT_RESET                       : out std_logic_vector(SIZE-1 downto 0)     := (others => '0')
);
end resetHandler;

architecture HandleResets of resetHandler is

    -- outside connections
    signal s_in_clk                 : std_logic                                 := '0';
    signal s_in_reset               : std_logic_vector(SIZE-1 downto 0)         := (others => '0');
    signal s_in_reset_global        : std_logic                                 := '0';
    signal s_out_reset              : std_logic_vector(SIZE-1 downto 0)         := (others => '0');
    
    -- internal connections
    signal s_reset_lock             : std_logic_vector(SIZE-1 downto 0)         := (others => '0');
    
begin

    s_in_clk                            <= IN_CLK;
    s_in_reset                          <= IN_RESET;
    s_in_reset_global                   <= IN_RESET_GLOBAL;

    ResetHandler : for i in 0 to SIZE-1 generate
    -- if a reset comes in and the lock is not engaged, send the reset out and lock the reset
    -- only unlock the reset again once the outside reset signal has gone back down
    process(s_in_clk) is
    begin
    if rising_edge(s_in_clk) then
        if (s_in_reset(i) = '1' or s_in_reset_global = '1') and s_reset_lock(i) = '0' then
            s_out_reset(i)              <= '1';
            s_reset_lock(i)             <= '1';
        elsif s_in_reset(i) = '0' and s_in_reset_global = '0' then
            s_reset_lock(i)             <= '0';
        end if;
        if s_out_reset(i) = '1' then
            s_out_reset(i)              <= '0';
        end if;
    end if;
    end process;
    end generate ResetHandler;
    
    OUT_RESET                           <= s_out_reset;

end HandleResets;
