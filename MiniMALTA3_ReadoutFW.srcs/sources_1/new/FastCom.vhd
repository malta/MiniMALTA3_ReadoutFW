----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 05/21/2024 11:35:56 AM
-- Design Name: 
-- Module Name: FastCom - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
library UNISIM;
use UNISIM.VComponents.all;
library unimacro;
use unimacro.vcomponents.all;
use work.ipbus.all;
use work.readout_constants.all;
use work.package_constants.all;


entity FastCom is
Port(
    -- control
    signal i_IPBeset        : in     std_logic;
    -- ipbus signals
    signal i_clk_ipb        : in     std_logic;                                       -- Connect a 32MHz clock
    signal i_ipbw           : in     ipb_wbus_array(99 downto 0);
    signal i_ipbr           : inout  ipb_rbus_array(99 downto 0);
    -- Module signals
    signal o_fastcom_reset  : out    std_logic; 
    signal i_clk_fastcom    : in     std_logic; 
    signal o_fastcom_serial : out    std_logic 
    
);
end FastCom;

architecture Behavioral of FastCom is

signal m_fastcom_reset        : std_logic := '1';
signal m_fastcom_data         : std_logic := '0';
signal m_fastcom_serial       : std_logic_vector(31 downto 0)  := (others => '0');
signal m_data_parallel        : std_logic_vector (23 downto 0)  := (others => '0');
signal m_fastcom_control      : std_logic_vector(31 downto 0)  := (others => '0');
signal m_counter_bitsInWord   : natural range 0 to 24 := 0; -- cycle 0 -> send 0, cycle NBITS+1 -> reset --481
signal m_load                 : std_logic := '0'; 

 
begin

 --- IPBUS Register: FastCom serial command register ---
reg_fastcom_serial: entity work.ipbus_register
port map(
   c_address  => std_logic_vector(to_unsigned(IPBADDR_FASTCOM_SERIAL,32)),
   i_ipbw     => i_ipbw(IPBADDR_FASTCOM_SERIAL),
   o_ipbr     => i_ipbr(IPBADDR_FASTCOM_SERIAL),
   i_clk      => i_clk_IPb,  
   i_reset    => i_IPBeset,
   c_readonly => '0',
   c_writeMon => '1',
   i_default  => X"00000000",
   o_signal   => m_fastcom_serial
 );
 
  --- IPBUS Register: FastCom serial command register ---
reg_fastcom_control: entity work.ipbus_register
port map(
   c_address  => std_logic_vector(to_unsigned(IPBADDR_FASTCOM_CONTROL,32)),
   i_ipbw     => i_ipbw(IPBADDR_FASTCOM_CONTROL),
   o_ipbr     => i_ipbr(IPBADDR_FASTCOM_CONTROL),
   i_clk      => i_clk_IPb,  
   i_reset    => i_IPBeset,
   c_readonly => '0',
   c_writeMon => '1',
   i_default  => X"00000000",
   o_signal   => m_fastcom_control
 );
 
 m_load <= m_fastcom_control(0);
 --- To reset the fast commander block you need to set the bit 1 to 0 as it is a reset low
 m_fastcom_reset <= m_fastcom_control(1);

process(i_clk_fastcom) is
begin
    if rising_edge(i_clk_fastcom) then
        if m_load ='1'and  m_counter_bitsInWord=0 then
            m_data_parallel <= m_fastcom_serial(23 downto 0);    
            m_fastcom_data <= m_data_parallel(m_counter_bitsInWord);
            m_counter_bitsInWord <= 1;
        elsif m_counter_bitsInWord=0 then
            m_fastcom_data <= '0';
        else
            if m_counter_bitsInWord=23 then
                m_counter_bitsInWord <= m_counter_bitsInWord+1;
                m_fastcom_data <= m_data_parallel(m_counter_bitsInWord);
            elsif m_counter_bitsInWord>23 then
                m_fastcom_data <= '0';
            else
                m_fastcom_data <= m_data_parallel(m_counter_bitsInWord);
                m_counter_bitsInWord <= m_counter_bitsInWord+1;
            end if;
        end if;
        if m_load = '0' then
            m_counter_bitsInWord <= 0;
        end if;
    end if;
end process; 
                    
o_fastcom_serial <= m_fastcom_data; 
o_fastcom_reset <= m_fastcom_reset; 
                
            
            
        
end Behavioral;
