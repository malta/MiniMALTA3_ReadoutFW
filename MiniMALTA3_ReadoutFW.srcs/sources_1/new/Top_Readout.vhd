----------------------------------------------------------------------------------
-- MALTA3Core
-- Adapted from MiniMaltaFullReadout_v6
-- Carlos.Solans@cern.ch
-- Ignacio.Asensi@cern.ch
-- October 2018
-- May 2020
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
library UNISIM;
use UNISIM.VComponents.all;
library unimacro;
use unimacro.vcomponents.all;
use work.ipbus.all;
use work.Readout_constants.all;

entity MALTA3Core is
 Port ( 
  --system clock
  i_sys_clk_P       : in    std_logic;
  i_sys_clk_N       : in    std_logic;
  --malta slow control pads
  o_rstB_TOMALTA    : out   std_logic; --c26  --LA27_P   --C19
  o_SC_CLK          : out   std_logic; --c14  --LA10_P   --D29
  o_SC_DATA         : out   std_logic; --c10  --LA06_P   --H30
  o_START           : out   std_logic; --d20  --LA17CC_P --F20  --There is no start signal in the new SlowControl
  i_SOUT            : in    std_logic;  --d23  --LA23_P   --B22 
  o_CHIPID          : out   std_logic_vector(2 downto 0); --This signal is not necessary in the SlowControl Block
  o_SC_LOAD         : out std_logic;
  o_REFPULSE_Valid  : out std_logic;
  i_DEBUG_DATA_PATH : in std_logic; 
  o_SRVpulse        : out std_logic;
  
  --leds/handles
  o_TOP_LEDS        : out   std_logic_vector(7 downto 0);
  i_SWI             : in    std_logic_vector(3 downto 0); 
  o_LPC_LEDS        : out   std_logic_vector(9 downto 0);
  i_PUSH_0          : in    std_logic;
  i_PUSH_1          : in    std_logic;
  i_PUSH_2          : in    std_logic;
  i_PUSH_3          : in    std_logic;
  i_PUSH_4          : in    std_logic;
  o_SMA_P           : out   std_logic; --unusable when FMC cable is connected
  i_SMA_N           : in    std_logic; 
  o_LPC_SMA         : out   std_logic_vector(9 downto 0);
  --o_VREF            : out   std_logic;
  --o_VREF1           : out   std_logic;
  --o_VPULSE          : out   std_logic;
  i_PLL_LOCK        : in    std_logic;
  o_SLOW_DATA       : out   std_logic;
  --for IPBUS
  SGMIICLK_Q0_P     : in    std_logic;
  SGMIICLK_Q0_N     : in    std_logic;
  SGMII_TX_P        : out   std_logic;
  SGMII_TX_N        : out   std_logic;
  SGMII_RX_P        : in    std_logic;
  SGMII_RX_N        : in    std_logic;
  PHY_RESET         : out   std_logic;
  -- for readout
  --o_CLK_640_P       : out   std_logic;
  --o_CLK_640_N       : out   std_logic;
  --o_CLK_40_P        : out   std_logic;
  --o_CLK_40_N        : out   std_logic;
  -- for bypass PLL clk to LAPA
  o_PLL_BYPASS_CLK_P       : out   std_logic;
  o_PLL_BYPASS_CLK_N       : out   std_logic;
  o_PLL_SOURCE_CLK_P       : out   std_logic;
  o_PLL_SOURCE_CLK_N       : out   std_logic;
  -- for FASTCOM module
  o_FASTCOM_SERIAL         : out   std_logic; 
  o_FASTCOM_RESET          : out   std_logic;
  i_FDOUT_P         : in    std_logic;
  i_FDOUT_N         : in    std_logic;    
  i_SDOUT_P         : in    std_logic;
  i_SDOUT_N         : in    std_logic;
  i_SDOUT_ACK_P     : in    std_logic;
  i_SDOUT_ACK_N     : in    std_logic
 );
end MALTA3Core;

architecture Behavioral of MALTA3Core is
 signal m_clk_PLL_BYPASS    : std_logic; --LF: This is the backup clock for the PLL
 signal m_clk_PLL_SOURCE    : std_logic;
 signal m_clk_640MHz        : std_logic;
 signal m_clk_640MHz_1      : std_logic;
 signal m_clk_out_640MHz    : std_logic;
 signal m_clk_600MHz        : std_logic;
 

 signal m_clk_200MHz        : std_logic;
 signal m_clk_150MHz        : std_logic;
 --signal m_clk_40MHz         : std_logic;
 signal m_clk_10MHz_SC      : std_logic;
 signal m_clk_30MHz         : std_logic;
 signal m_clk_out_sc_30MHz  : std_logic;
 signal m_clk_40MHz         : std_logic; 
 signal m_clk_5MHz          : std_logic;
 signal m_clk_50MHz         : std_logic;
 signal m_clk_26_25MHz         : std_logic;
 signal m_clk_32MHz         : std_logic;
 signal m_clk_IPbus         : std_logic;
 signal m_TMPsysCLK         : std_logic;
 signal enabled_SC_Clk      : std_logic;
 -- Malta
 signal m_vpulse            : std_logic;
 signal m_vref              : std_logic;
 signal m_rstB              : std_logic := '0';
 signal m_pll_lock          : std_logic;
 signal m_slow_flag         : std_logic;
 --signal m_enable            : std_logic;
 signal m_enable_SCCLK      : std_logic;
 signal m_enable_FROCLK     : std_logic;
 signal m_enable_SROCLK     : std_logic;
 signal m_start             : std_logic;
 signal m_data_toMalta      : std_logic;
 signal m_sout              : std_logic;
 signal m_sout_ack          : std_logic;
 signal m_sout2             : std_logic;
 --signal m_sout2_ack         : std_logic;
 signal m_reading           : std_logic;
 signal m_SC_error          : std_logic := '0';
 -- debug
 signal m_LEDS              : std_logic_vector(7 downto 0) := (others => '0');
 signal m_LPC_LEDS          : std_logic_vector(9 downto 0) := (others => '1');
 signal m_HPC_LEDS          : std_logic_vector(9 downto 0) := (others => '0');
 -- version
 signal m_version           : std_logic_vector(31 downto 0) := X"00000013"; 
 signal m_monitor           : std_logic_vector(31 downto 0) := X"00000000";
 -- IPbus variables
 signal m_sys_rst           : std_logic := '0';
 signal m_control             : std_logic_vector(31 downto 0) := X"00000000";
 signal m_ipbw              : ipb_wbus_array(99 downto 0);
 signal m_ipbr              : ipb_rbus_array(99 downto 0);
 signal m_reset           : std_logic                     := '0';
 -- signals for readout
 signal m_ro_status         : std_logic_vector(15 downto 0);
 signal m_l1a_sma           : std_logic                     := '0';
 signal m_l1a_fmc           : std_logic                     := '0';    
 signal m_l1a_reset         : std_logic                     := '0';
 signal m_miniMaltaData     : std_logic;
 signal m_data_select       : std_logic;
 signal m_fast_data         : std_logic;
 signal m_slow_data         : std_logic;
 signal m_slow_data2        : std_logic;
 signal m_slow_ack          : std_logic;
 signal m_single_fast_bit   : std_logic;
 signal m_fast_data_p       : std_logic;
 signal m_fast_data_n       : std_logic;
 signal m_slow_data_p       : std_logic;
 signal m_slow_data_n       : std_logic;
 signal m_slow_ack_p        : std_logic;
 signal m_slow_ack_n        : std_logic;
 signal m_SRVpulse          : std_logic;
 signal i_switch_count      : integer range 0 to 9 := 0;
 signal m_MaltaSC_ClkEnable : std_logic:='0';
 signal m_SC_load           : std_logic:='0';
 -- signals to drive 640MHz and 40MHz diff clocks to MALTA
 signal m_clk_640MHz_p      : std_logic;
 signal m_clk_640MHz_n      : std_logic;
 signal m_clk_40MHz_p       : std_logic;
 signal m_clk_40MHz_n       : std_logic;
 -- signal to drive the backup clock for LAPA (bypass PLL)
 signal m_clk_PLL_BYPASS_p      : std_logic;
 signal m_clk_PLL_BYPASS_n      : std_logic; 
 signal m_clk_PLL_SOURCE_p      : std_logic;
 signal m_clk_PLL_SOURCE_n      : std_logic; 
 signal m_clk_160Mhz_disable          : std_logic := '0';
 -- for timing
 signal m_fast_counter      : std_logic_vector(32 downto 0) := (others => '0');
 signal m_ipb_L1            : std_logic := '0';
 signal m_real_L1           : std_logic := '0';
 signal m_fifoEnable        : std_logic := '0';
 signal m_L1A_counter       : std_logic_vector(11 downto 0) := (others => '0');
 signal m_BCID_counter      : std_logic_vector(15 downto 0) := (others => '0');
 signal m_debug             : std_logic := '0';
 signal m_debug2            : std_logic := '0';
 signal m_debugRO           : std_logic := '0';
 signal m_debugRO2          : std_logic := '0';
 signal m_LPC_SMA           : std_logic_vector(9 downto 0) := (others => '0');
 signal m_fastSignal        : std_logic := '0';
 -- MALTA2 only
 signal m_chipid_reg         : std_logic_vector(31 downto 0) := X"00000000";
 signal m_load_E_reg         : std_logic_vector(31 downto 0) := X"00000000";
 signal m_REFPULSE_Valid_IPBUS     : std_logic := '0';
 signal m_REFPULSE_Valid_Pulser     : std_logic := '0';
 signal m_REFPULSE_Valid    : std_logic := '0';
 signal m_debug_word_hit    : std_logic:='0';
 signal m_fastcom_serial    : std_logic := '0';
 signal m_fastcom_reset     : std_logic := '1';
 
 
 component Clock_1 is
   port(
     clk_in1         : in  std_logic := '0';
     clk_10MHz_SC    : out std_logic := '0';
     clk_PLL_BYPASS  : out std_logic := '0';
     clk_PLL_SOURCE  : out std_logic := '0';
     clk_40MHz       : out std_logic := '0';
     clk_5MHz       : out std_logic := '0';
     clk_32MHz       : out std_logic := '0'
   );
 end component Clock_1;
 
 component Clock_2 is
   port(
     clk_in1     : in  std_logic := '0';
     clk_200MHz  : out std_logic := '0';
     clk_30MHz   : out std_logic := '0';
     clk_50MHz   : out std_logic := '0';
     clk_26_25MHz   : out std_logic := '0'
   );
 end component Clock_2;

 begin
 -- leds for debugging  
 o_TOP_LEDS            <= m_LEDS;
 o_LPC_LEDS            <= m_LPC_LEDS;
 
 -- input clk
 inL1: IBUFGDS
  port map(
   O=>m_TMPsysCLK,
   I=>i_SYS_CLK_P,
   IB=>i_SYS_CLK_N
  );
 clk1: Clock_1
  port map(
    clk_in1             => m_TMPsysCLK,
    clk_10MHz_SC        => m_clk_10MHz_SC,
    clk_PLL_BYPASS      => m_clk_PLL_BYPASS,
    clk_PLL_SOURCE      => m_clk_PLL_SOURCE,
    clk_40MHz           => m_clk_40MHz,
    clk_5MHz            => m_clk_5MHz,
    clk_32MHz           => m_clk_32MHz
  );
 -- m_clk_out_640MHz       <= m_clk_640MHz and m_enable_FROCLK;
 
 clkOut: ODDR
  port map (
    Q  => m_clk_out_640MHz,
    C  => m_clk_640MHz,
    CE => m_enable_FROCLK, 
    D1 => '1',
    D2 => '0'
    --.R(R),   // 1-bit reset
    --.S(S)    // 1-bit set
 ); 
 clk2: Clock_2
 port map(
    clk_in1             => m_TMPsysCLK,
    clk_200MHz          => m_clk_200MHz,
    clk_30MHz           => m_clk_30MHz,
    clk_50MHz           => m_clk_50MHz,
    clk_26_25MHz        => m_clk_26_25MHz
 );
 --m_clk_out_ro_40MHz        <= (not m_clk_40MHz) and m_enable_SROCLK;
 --m_clk_out_sc_30MHz        <= (not m_clk_30MHz) and m_enable_SCCLK;

 --------------------------------------------------------------------------------------------------------------------------------------------------------------------
 --------------------------------------------------------------------------------------------------------------------------------------------------------------------
 -- IPBUS top
 ipbus: entity work.ipbus_top
 generic map(NSLV => NSLAVES )
 port map(
   ipb_to_slaves    => m_ipbw,
   ipb_from_slaves  => m_ipbr,
   i_addrSwi        => i_SWI,
   i_clk200         => m_clk_200MHz,
   o_ipb_clk        => m_clk_IPbus,
   i_sys_rst        => m_sys_rst,
--   o_clk1hz=>open, o_clk_locked=>m_LEDS(6), o_eth_locked=>m_LEDS(7),
   o_clk1hz         => open,
   o_clk_locked     => open, 
   o_eth_locked     => open,
   o_ipb_rx_led     => open,
   o_ipb_tx_led     => open,
   i_gt_clkp        => SGMIICLK_Q0_P,
   i_gt_clkn        => SGMIICLK_Q0_N,
   o_gt_txp         => SGMII_TX_P   , 
   o_gt_txn         => SGMII_TX_N,
   i_gt_rxp         => SGMII_RX_P   , 
   i_gt_rxn         => SGMII_RX_N
 );  
 
 m_sys_rst <= I_PUSH_0;  -- VD could be better improved with the reset!!!
 PHY_RESET <= not m_sys_rst ;

 -- Register: Version
 version: entity work.ipbus_register
 port map(
   c_address  => std_logic_vector(to_unsigned(IPBADDR_VERSION,32)),
   i_ipbw     => m_ipbw(IPBADDR_VERSION),
   o_ipbr     => m_ipbr(IPBADDR_VERSION),
   i_clk      => m_clk_IPbus, 
   i_reset    => m_sys_rst,
   c_readonly => '1', 
   c_writeMon => '1',
   i_default  => m_version,
   o_signal => open
 );   



--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
OBUFDS_PLL_BYPASS : OBUFDS
 generic map (
  IOSTANDARD => "DEFAULT", 
  SLEW       => "FAST")    
 port map (
  O  => m_clk_PLL_BYPASS_p,
  OB => m_clk_PLL_BYPASS_n,
  I  => m_clk_PLL_BYPASS
 );
 o_PLL_BYPASS_CLK_P        <= m_clk_PLL_BYPASS_p;
 o_PLL_BYPASS_CLK_N        <= m_clk_PLL_BYPASS_n;  
 
 
 
 
 
 
--    I  => m_clk_PLL_SOURCE
 
 OBUFDS_PLL_SOURCE : OBUFDS
  generic map (
   IOSTANDARD => "DEFAULT", 
   SLEW       => "FAST")    
  port map (
   O  => m_clk_PLL_SOURCE_p,
   OB => m_clk_PLL_SOURCE_n,
   I  => m_clk_PLL_SOURCE
   
   
   
  );
  o_PLL_SOURCE_CLK_P        <= m_clk_PLL_SOURCE_p;
  o_PLL_SOURCE_CLK_N        <= m_clk_PLL_SOURCE_n;  


--------------------------------------------------------------------------------------------------------------------------------------------------------------

  o_CHIPID <= "000";
  o_SC_LOAD <= m_SC_load;
  o_rstB_TOMALTA  <=  m_rstB;
  --resetButton_Buffer <= (not i_PUSH_0);
  o_SLOW_DATA     <= '0';--m_slow_flag;
  m_pll_lock      <= i_PLL_LOCK;
  m_vref          <= '0';
  --o_SMA_P      <= m_data_toMalta;



o_LPC_SMA    <= m_LPC_SMA;
o_LPC_SMA (7) <= m_debug_word_hit;
--o_LPC_SMA (8) <= m_REFPULSE_Valid_IPBUS;
--o_LPC_SMA (8) <= m_REFPULSE_Valid;
--o_LPC_SMA (8) <= m_SRVpulse;
o_LPC_SMA (8) <= m_fastcom_serial;
o_LPC_SMA (9) <= m_fastcom_reset;
--o_LPC_SMA (9) <= i_DEBUG_DATA_PATH;
o_LPC_LEDS    <=  m_LPC_LEDS;
    
OBUF_start : OBUF
  generic map (
    --DRIVE => 8,
    IOSTANDARD => "DEFAULT"
    --SLEW => "FAST"
    )
    port map (
    O => o_START,
    I => m_start
  );
  
OBUF_scclk : OBUF
  generic map (
    --DRIVE => 4,
    IOSTANDARD => "DEFAULT"
    --SLEW => "FAST"
    )
    port map (
    O => o_SC_CLK,
    I => m_clk_10MHz_SC          -- Changed by LF it was m_clk_40MHz
  );
  
  
  enabled_SC_Clk <= m_clk_10MHz_SC and m_MaltaSC_ClkEnable;
  
  OBUF_scdata : OBUF
  generic map (
    --DRIVE => 8,
    IOSTANDARD => "DEFAULT"
    --SLEW => "FAST"
  )
    port map (
    O => o_SC_DATA,
    I => m_data_toMalta
  );

  IBUF_SDATA: IBUF
  generic map(
    IOSTANDARD => "DEFAULT",
    IBUF_LOW_PWR => True
  )
  port map(
    O => m_sout2,
    I => i_SOUT
  );
  
 
  ----------- PERIODIC PULSING
  
      OBUF_REFPulse : OBUF
    generic map (
      DRIVE => 8,
      IOSTANDARD => "DEFAULT",
      SLEW => "FAST"
    )
      port map (
      O => o_REFPULSE_Valid,
      I => m_REFPULSE_Valid
    );
    
    
    
    
      ----------- MATRIX PULSING
    
        OBUF_SCVPulse : OBUF
      generic map (
        DRIVE => 8,
        IOSTANDARD => "DEFAULT",
        SLEW => "FAST"
      )
        port map (
        O => o_SRVpulse,
        I => m_SRVpulse
      );


--    o_REFPULSE_Valid <= m_REFPULSE_Valid_Pulser;
  
    
   SCV_PULSE: entity work.Periodic_pulser 
   port map (
      i_clock              => m_clk_40MHz,
      o_pulse_signal       => m_REFPULSE_Valid_Pulser
    );
  
  
--m_REFPULSE_Valid <= m_REFPULSE_Valid_Pulser and m_REFPULSE_Valid_IPBUS;

m_SRVpulse <= m_REFPULSE_Valid_Pulser and m_REFPULSE_Valid_IPBUS;

--- FASTCOM SERIAL buffer 

      OBUF_FastCom : OBUF
      generic map (
        DRIVE => 8,
        IOSTANDARD => "DEFAULT",
        SLEW => "FAST"
      )
        port map (
        O => o_FASTCOM_SERIAL, 
        I => m_fastcom_serial
      );

--- FASTCOM SERIAL buffer 

      OBUF_FastReset : OBUF
      generic map (
        DRIVE => 8,
        IOSTANDARD => "DEFAULT",
        SLEW => "FAST"
      )
        port map (
        O => o_FASTCOM_RESET, 
        I => m_fastcom_reset
      );


 --------------------------------------------------------------------------------------------------------------------------------------------------------------
  MiniMALTA3_Slow_Control: entity work.Slow_Control_top
 port map( 
   -- slow control ports
   o_REFPULSE_ValidIn => m_REFPULSE_Valid_IPBUS,
   o_SC_loadout       => m_SC_load,
   o_SCclk_enable   => m_MaltaSC_ClkEnable, 
   i_clk_sc         => m_clk_10MHz_SC, -- Clock
   i_sout           => m_sout2,        -- Serial stream slowcontrol from Malta2
   o_rstB           => m_rstB,         -- High when sending serialized data. Not needed by the chip?
   o_data_toMalta   => m_data_toMalta, -- Serial stream slowcontrol to Malta2 
   -- ipbus signals
   i_IPBeset        => m_sys_rst,
   i_clk_ipb        => m_clk_IPbus,         
   i_ipbw           => m_ipbw,
   i_ipbr           => m_ipbr,
   -- debug leds
--   o_LPC_LEDS       => m_LPC_LEDS ,     -- Needed if you want debug in SC
   o_LPC_SMA        => m_LPC_SMA        -- Needed 
   
 );

 --------------------------------------------------------------------------------------------------------------------------------------------------------------
  MiniMALTA3_slowdata: entity work.MiniMALTA3_slowdata
 port map( 
  -- ipbus signals
   i_clk_ipb        => m_clk_IPbus,         
   i_ipbw           => m_ipbw,
   i_ipbr           => m_ipbr,
   i_IPBeset        => m_sys_rst,
   -- other
   i_debug_data_path_readout => i_DEBUG_DATA_PATH,
   m_clk_serializer      =>  m_clk_32MHz,
   --m_clk_serializer      =>  m_clk_26_25MHz,
   -- debug
   o_LPC_LEDS       => m_LPC_LEDS ,     -- Needed if you want debug in SC
  -- o_LPC_SMA        => m_LPC_SMA        -- Needed 
    o_word_hit  => m_debug_word_hit
   
 );
 
  --------------------------------------------------------------------------------------------------------------------------------------------------------------
  MiniMALTA3_FastCom: entity work.FastCom
  port map(
     i_IPBeset        => m_sys_rst,  
     i_clk_ipb        => m_clk_IPbus,         
     i_ipbw           => m_ipbw,
     i_ipbr           => m_ipbr,
     o_fastcom_reset  => m_fastcom_reset,
     i_clk_fastcom    => m_clk_32MHz, 
     o_fastcom_serial => m_fastcom_serial
  
  );
 
end Behavioral;
