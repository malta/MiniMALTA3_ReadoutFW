library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
library UNISIM;
use UNISIM.VComponents.all;
use work.ipbus.all;
use work.readout_constants.all;

entity timestampHandler is
port(
    IN_CLK                                  :  in std_logic                         := '0';
    IN_DATA                                 :  in std_logic_vector(47 downto 0)     := (others => '0');
    IN_NEW_DATA                             :  in std_logic                         := '0';
    IN_L1A_COUNT                            :  in std_logic_vector(11 downto 0)     := (others => '0');
    IN_BCID_COUNT                           :  in std_logic_vector(14 downto 0)     := (others => '0');
    OUT_DATA                                : out std_logic_vector(61 downto 0)     := (others => '0');
    OUT_NEW_DATA                            : out std_logic                         ;
    OUT_DEBUG                               : out std_logic_vector( 3 downto 0)     := (others => '0')
);
end timestampHandler;

architecture Behavioral of timestampHandler is
    -- some types for pipelining
    type int12_array is array(1 downto 0) of integer range 0 to 4095;
    type lgc48_array is array(1 downto 0) of std_logic_vector(47 downto 0);

    -- interface
    signal s_in_clk                         : std_logic                             := '0';
    signal s_in_data                        : lgc48_array                           := (others => (others => '0'));
    signal s_in_new_data                    : std_logic                             := '0';
    signal s_out_data                       : std_logic_vector(61 downto 0)         := (others => '0');
    signal s_out_new_data                   : std_logic                             := '0';
    signal s_out_debug                      : std_logic_vector( 3 downto 0)         := (others => '0');
    
    signal i_in_l1a                         : int12_array                           := (others => 0);
    signal i_in_trigger_bcid                : integer range 0 to 32768              := 0;
    signal i_in_miniMalta_bcid              : integer range 0 to 32768              := 0;
    
    -- FSM
    signal s_new_frame                      : std_logic                             := '0';
    signal s_lock_new_data                  : std_logic                             := '0';
    signal s_new_bcid                       : std_logic_vector(14 downto 0)         := (others => '0');
    signal s_new_l1a                        : std_logic_vector(11 downto 0)         := (others => '0');
    signal s_data_ready                     : std_logic                             := '0';
    
    signal i_trigger_base_bcid              : integer range 0 to 32768              := 0;    
    signal i_miniMalta_base_bcid            : integer range 0 to 32768              := 0;
    signal i_rel_bcid_count                 : integer range 0 to 32768              := 0;

begin
    
    ---------------------------------------------------------------------------------------------------
    -- hook up inputs
    s_in_clk                                <= IN_CLK;
    s_in_data(0)                            <= IN_DATA;
    s_in_new_data                           <= IN_NEW_DATA;
    i_in_l1a(0)                             <= to_integer(unsigned(IN_L1A_COUNT));
    i_in_trigger_bcid                       <= to_integer(unsigned(IN_BCID_COUNT));
    i_in_miniMalta_bcid                     <= to_integer(unsigned(IN_DATA(14 downto 0)));

    ---------------------------------------------------------------------------------------------------
    -- if L1A changes (= is icreased) load new T0 BCID
    -- if DATA changes, calculate relative BCID, create new data word and pipe out
    
    calculateTimestamps: process(s_in_clk) is
    begin
    if rising_edge(s_in_clk) then
        -- look for new frames
        if i_in_trigger_bcid = 0 then
            s_new_frame                     <= '1';
        end if;
        
        if s_in_new_data = '1' then
            -- at some point new data arrives
            if s_new_frame = '1' then
                -- if it is the first word load new base bcids for trigger and miniMalta and calculate rel. bcid
                s_new_frame                 <= '0';
                i_trigger_base_bcid         <= i_in_trigger_bcid;
                i_miniMalta_base_bcid       <= i_in_miniMalta_bcid;
                i_rel_bcid_count            <= i_in_trigger_bcid;
            else
                -- else calculate the rel_bcid based on the base bcids
                if i_in_miniMalta_bcid >= i_miniMalta_base_bcid then
                    i_rel_bcid_count        <= i_trigger_base_bcid+i_in_miniMalta_bcid-i_miniMalta_base_bcid;
                else
                    i_rel_bcid_count        <= i_trigger_base_bcid+i_in_miniMalta_bcid-i_miniMalta_base_bcid+32768;
                end if;
            end if;
            -- cache miniMaltaData and l1a
            s_in_data(1)                    <= s_in_data(0);
            i_in_l1a(1)                     <= i_in_l1a(0);
            s_data_ready                    <= '1';
        end if;
        
        if s_data_ready = '1' then
            s_data_ready                    <= '0';
            s_out_new_data                  <= '1';
            s_out_data                      <= "00" &
                                               std_logic_vector(to_unsigned(i_in_l1a(1),12)) &
                                               s_in_data(1)(47 downto 15) &
                                               std_logic_vector(to_unsigned(i_rel_bcid_count,15));
        end if;
        
        if s_out_new_data = '1'then
            s_out_new_data                  <= '0';
        end if;
        
    end if;
    end process calculateTimestamps;
    
--    calculateTimestamps: process(s_in_clk) is
--    begin
--    if rising_edge(s_in_clk) then
    
--        ----------- CLK N -----------
--        -- look for new frames
--        if i_in_trigger_bcid(0) = 0 then
--            s_new_frame                     <= '1';
--        end if;
        
--        -- always calculate the new relative BCID count, this should be in sync with the new_frame flag
--        if i_in_miniMalta_bcid(0) >= i_miniMalta_base_bcid then
--            i_rel_bcid_count                <= i_trigger_base_bcid+i_in_miniMalta_bcid(0)-i_miniMalta_base_bcid;
--        else
--            i_rel_bcid_count                <= i_trigger_base_bcid+i_in_miniMalta_bcid(0)-i_miniMalta_base_bcid+16384;
--        end if;
----        i_rel_bcid_count                <= i_trigger_base_bcid+i_in_miniMalta_bcid(0)-i_miniMalta_base_bcid;
--        -- buffer input counters for 1 clk cycle
--        i_in_l1a(1)                         <= i_in_l1a(0);
--        i_in_trigger_bcid(1)                <= i_in_trigger_bcid(0);
--        i_in_miniMalta_bcid(1)              <= i_in_miniMalta_bcid(0);
        
--        ---------- CLK N+1 ----------
--        -- in_new_data runs at ~150MHz and is much slower than in_clk, add a lock flag to prevent multiple reads of the same data
--        -- the input counters from the previous clock cycle must be used to stay in sync with the rel_BCID and the new_frame flag
--        if s_in_new_data = '1' and s_lock_new_data = '0' then
--            s_lock_new_data                 <= '1';
--            if s_new_frame = '1' then
--                s_new_frame                 <= '0';
--                -- update base counters and pipe out word at the same time
--                -- read counters from entry 0
--                i_trigger_base_bcid         <= i_in_trigger_bcid(1);
--                i_miniMalta_base_bcid       <= i_in_miniMalta_bcid(1);
--                s_out_new_data              <= '1';
--                s_out_data                  <= "00" &
--                                               std_logic_vector(to_unsigned(i_in_l1a(1),12)) &
--                                               s_in_data(47 downto 14) &
--                                               std_logic_vector(to_unsigned(i_in_trigger_bcid(1),14));
--            else
--                s_out_new_data              <= '1';
--                s_out_data                  <= "00" &
--                                               std_logic_vector(to_unsigned(i_in_l1a(1),12)) &
--                                               s_in_data(47 downto 14) &
--                                               std_logic_vector(to_unsigned(i_rel_bcid_count,14));
--            end if;
--        elsif s_in_new_data = '0' then
--            s_lock_new_data                 <= '0';
--        end if;
--        if s_out_new_data = '1' then
--            s_out_new_data                  <= '0';
--        end if;
--    end if;
--    end process calculateTimestamps;
    
    ---------------------------------------------------------------------------------------------------
    -- hook up outputs
    
    s_out_debug(0)                          <= s_new_frame;
    s_out_debug(1)                          <= s_in_new_data;
    s_out_debug(2)                          <= s_out_new_data;
    s_out_debug(3)                          <= s_in_clk;
    
    OUT_DATA                                <= s_out_data;
    OUT_NEW_DATA                            <= s_out_new_data;
    OUT_DEBUG                               <= s_out_debug;
    ---------------------------------------------------------------------------------------------------

end Behavioral;













--architecture Valerio of timestampHandler is
--    signal s_in_clk                         : std_logic                             := '0';   
--    signal s_in_new_data                    : std_logic                             := '0';

--    signal s_out_data                       : std_logic_vector(61 downto 0)         := (others => '0');
--    signal s_out_new_data                   : std_logic                             := '0';
--    signal s_out_debug                      : std_logic_vector( 3 downto 0)         := (others => '0');

--begin

--    s_in_clk                                <= IN_CLK;
--    s_in_new_data                           <= IN_NEW_DATA;
    
--    calculateTimestamps: process(s_in_clk) is
--    begin
--    if rising_edge(s_in_clk) then
--        s_out_new_data <= s_in_new_data;
--        if s_in_new_data='1' then
--            s_out_data(14 downto 0)  <= IN_BCID_COUNT;
--            s_out_data(47 downto 15) <= IN_DATA(47 downto 15);
--            s_out_data(59 downto 48) <= IN_L1A_COUNT;
            
--        end if;
--    end if;
--    end process calculateTimestamps;
    
--    s_out_debug(0)                          <= '1';--s_new_frame;
--    s_out_debug(1)                          <= s_in_new_data;
--    s_out_debug(2)                          <= s_out_new_data;
--    s_out_debug(3)                          <= s_in_clk;    
--    OUT_DATA                                <= s_out_data;
--    OUT_NEW_DATA                            <= s_out_new_data;
--    OUT_DEBUG                               <= s_out_debug;

--end Valerio;