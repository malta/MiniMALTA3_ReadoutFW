library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.package_constants.all;
use work.readout_constants.all;

----------------------------------------------------

entity slowdata_deserializer is
port(
    i_clock        : in  std_logic;
    o_dataParallel : out std_logic_vector(63 downto 0);
    i_dataSerial   : in  std_logic;
    o_busy         : out std_logic;
    o_send         : out  std_logic 
);
end slowdata_deserializer;

architecture slowdata_deserializer of slowdata_deserializer is

    signal m_o_busy         : std_logic := '0';
    signal m_i_dataSerial   : std_logic := '0';
    signal m_o_dataParallel : std_logic_vector(63 downto 0):= (others => '0');
    --signal m_temp_dataParallel : std_logic_vector(63 downto 0):= (others => '1');
    signal m_word_out   : std_logic := '0';
    signal m_counter : natural range 0 to 63 := 0;
    signal m_active : std_logic; 
    signal m_dataSerial_temp :std_logic := '0';
begin
    
    o_busy         <= m_o_busy;
    o_dataParallel <= m_o_dataParallel;
    o_send <= m_word_out;
     process_deserializer : process (i_clock) is
       begin
           if rising_edge(i_clock) then
            m_dataSerial_temp <= i_dataSerial;
            if (i_dataSerial = '1') then
                m_active <='1';
            end if;
            if (m_active='1') then 
                m_counter<= m_counter+1;
                if (m_counter = 63) then
                    m_word_out <='1';
                    m_active<='0';
                    m_counter <=0;
                elsif (m_counter < 63) then
                    m_word_out <='0';
                end if;
                m_o_dataParallel(m_counter) <= m_dataSerial_temp;
            else
                m_o_dataParallel<=(others => '0');
                m_word_out <='0';
            end if;
           end if;
       end process process_deserializer;
--    process_deserializer : process (i_clock) is
--    begin
--        if rising_edge(i_clock) then
--            m_o_dataParallel <= m_temp_dataParallel(m_temp_dataParallel'high-1 downto 0) & i_dataSerial;
--            if (m_o_dataParallel(10 downTo 0) = "11111000000") then 
--                m_word_out <='1';
--            else
--                m_word_out <='0';
--            end if;
--        end if;
--    end process process_deserializer;
end slowdata_deserializer;
