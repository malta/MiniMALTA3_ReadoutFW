----------------------------------------------------------------------------------
-- MiniMALTA3 Slow Data
-- Ignacio Asensi
-- Leyre Flores
--  April 2024
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
library UNISIM;
use UNISIM.VComponents.all;
library unimacro;
use unimacro.vcomponents.all;
use work.ipbus.all;
use work.readout_constants.all;
use work.package_constants.all;

entity MiniMALTA3_slowdata is
    Port (
        -- ipbus signals
        signal i_clk_ipb        : in     std_logic;                                       -- Connect a 32MHz clock
        signal i_ipbw           : in     ipb_wbus_array(99 downto 0);
        signal i_ipbr           : inout  ipb_rbus_array(99 downto 0);
        signal i_IPBeset        : in    std_logic;
           
        signal i_debug_data_path_readout: in std_logic;
        signal m_clk_serializer      : in std_logic;
        signal m_slowdata_word  : out std_logic_vector(63 downto 0);
        
        signal o_LPC_LEDS       : out    std_logic_vector(9 downto 0);
        signal o_word_hit : out std_logic
     );
end MiniMALTA3_slowdata;

architecture Behavioral of MiniMALTA3_slowdata is

signal m_in_data	 :std_logic := '0';
signal m_word_send   :  std_logic;



-- FIFO signals
signal m_fifo_reset       : std_logic := '0';
signal m_64bitWORD        : std_logic_vector(63 downto 0) := (others => '0');
signal m_fifo_wren        : std_logic := '0';
signal m_32bitWORD_reg    : std_logic_vector(31 downto 0) := (others => '0');
signal m_fifo_rdclk       : std_logic := '0';
signal m_fifo_almostFull  : std_logic := '1';
signal m_rd_data_count    : std_logic_vector(8 downto 0) := (others => '0');
signal m_wr_data_count    : std_logic_vector(7 downto 0) := (others => '0');

signal m_slowdata_mon    : std_logic_vector(31 downto 0) := (others => '0');

-- ipbus signals
signal m_fifo_empty        :std_logic := '0';
signal m_fifo_full         :std_logic := '1';
signal m_fifo_rden         :std_logic := '0';

component slowdata_fifo is
    port (
        rst           : IN STD_LOGIC;
        wr_clk        : IN STD_LOGIC;
        rd_clk        : IN STD_LOGIC;
        din           : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
        wr_en         : IN STD_LOGIC;
        rd_en         : IN STD_LOGIC;
        dout          : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
        full          : OUT STD_LOGIC;
        almost_full   : OUT STD_LOGIC;
        empty         : OUT STD_LOGIC;
        rd_data_count : OUT STD_LOGIC_VECTOR(8 DOWNTO 0);
        wr_data_count : OUT STD_LOGIC_VECTOR(7 DOWNTO 0)
    );
end component slowdata_fifo;

begin
slowdata_read_FIFO : slowdata_fifo
  port map(
      rst           => m_fifo_reset,
      wr_clk        => m_clk_serializer,
      rd_clk        => i_clk_IPb,
      din           => m_64bitWORD,
      wr_en         => m_fifo_wren,
      rd_en         => m_fifo_rden,
      dout          => m_32bitWORD_reg ,
      full          => m_fifo_full,
      almost_full   => m_fifo_almostFull,
      empty         => m_fifo_empty,
      rd_data_count => m_rd_data_count,
      wr_data_count => m_wr_data_count);
  
slowdata_read_ipbusreg: entity work.ipbus_fifo
generic map(c_ok_in_msb => '0')
port map(
    c_address  => std_logic_vector(to_unsigned(IPBADDR_SLOWDATA,32)), 
    i_clk      => i_clk_IPb,
    i_ipbw     => i_ipbw(IPBADDR_SLOWDATA),
    o_ipbr     => i_ipbr(IPBADDR_SLOWDATA),
    c_readonly => '1',
    i_empty    => m_fifo_empty,
    i_full     => m_fifo_full, 
    i_rddata   => m_32bitWORD_reg,
    o_rdenable => m_fifo_rden,
    o_wrdata   => open, 
    o_wrclk    => open, 
    o_wrenable => open ); 
 
deserializer: entity work.slowdata_deserializer
 port map(
   i_clock        => m_clk_serializer ,
   i_dataSerial   => i_debug_data_path_readout,
   o_dataParallel => m_64bitWORD,
   o_send         => m_fifo_wren  );

 slowdata_monitoring: entity work.ipbus_register
 port map(
   c_address  => std_logic_vector(to_unsigned(IPBADDR_SLOWDATA_MON,32)),
   i_ipbw     => i_ipbw(IPBADDR_SLOWDATA_MON),
   o_ipbr     => i_ipbr(IPBADDR_SLOWDATA_MON),
   i_clk      => i_clk_IPb, 
   i_reset    => i_IPBeset,
   c_readonly => '1', 
   c_writeMon => '1',
   i_default  => m_slowdata_mon,
   o_signal => open); 
 
o_LPC_LEDS(7 downTo 0) <= m_wr_data_count;

m_slowdata_mon( 8 downTo 0) <= m_rd_data_count;
m_slowdata_mon( 16 downTo 9)<= m_wr_data_count;
o_word_hit <= m_fifo_wren;
end Behavioral;
