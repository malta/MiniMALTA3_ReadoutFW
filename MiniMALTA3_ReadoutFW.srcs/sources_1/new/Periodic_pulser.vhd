----------------------------------------------------------------------------------
-- MiniMALTA3 Pulsing signal
-- Leyre Flores
-- Description: Debug way to send a pulse signal to the chip via the frimware          
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Periodic_pulser is
generic(
   PULSE_SIGNAL_LENGTH  : natural   :=1;  
   PULSE_SIGNAL_PERIOD  : natural   :=20000
);
port (
  i_clock               : in  std_logic;  -- CLK provided by the PLL
  o_pulse_signal        : out std_logic
--  i_start_pulsing       : in  std_logic;  -- It will be done with 
);
end Periodic_pulser;

architecture Behavioral of Periodic_pulser is

-- signal m_start_pulsing  : std_logic := '0';
 signal m_pulse_ongoing  : std_logic := '0'; 
 signal m_pulse_signal   : std_logic := '0'; 
 signal m_counter        : natural range 0 to PULSE_SIGNAL_PERIOD := 0; 

begin

--m_start_pulsing <= i_start_pulsing; 
 
process_pulse_signal : process (i_clock) is
begin
   if rising_edge(i_clock) then
      if  (m_pulse_ongoing = '0') then 
          m_pulse_signal <= '1';
          m_pulse_ongoing <= '1'; 
          m_counter <= 0; 
      elsif (m_pulse_ongoing = '1') and (m_counter < PULSE_SIGNAL_LENGTH) then
          m_pulse_signal <= '1';
          m_pulse_ongoing <= '1'; 
          m_counter <= m_counter + 1;
      elsif (m_pulse_ongoing = '1') and (m_counter = PULSE_SIGNAL_LENGTH) then
          m_pulse_signal <= '0';
          m_pulse_ongoing <= '1';           
          m_counter <= m_counter+1; 
      elsif (m_counter < PULSE_SIGNAL_PERIOD) and (m_counter > PULSE_SIGNAL_LENGTH) and (m_pulse_ongoing = '1') then
          m_pulse_ongoing <= '1'; 
          m_counter <= m_counter+1;
      elsif (m_counter = PULSE_SIGNAL_PERIOD) and (m_pulse_ongoing = '1') then
          m_counter <= 0;
          m_pulse_signal <= '0';          
          m_pulse_ongoing <= '0';
  
      end if;
   end if;
end process process_pulse_signal;

  o_pulse_signal <= m_pulse_signal; 
  
end Behavioral;