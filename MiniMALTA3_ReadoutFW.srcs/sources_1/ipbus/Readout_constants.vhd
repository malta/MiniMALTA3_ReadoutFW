library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

package readout_constants is

 constant NSLAVES                   : positive := 100;
 
 constant IPBADDR_VERSION           : natural :=  0;
 constant IPBADDR_CONTROL           : natural :=  1;
 constant IPBADDR_FIFO              : natural :=  2;
 constant IPBADDR_SC_RESET          : natural :=  4;
 constant IPBADDR_TIMINGCONTROL     : natural :=  3;
 
 constant IPBADDR_SLOWDATA          : natural :=  5;
 constant IPBADDR_SLOWDATA_MON      : natural :=  6;
 constant IPBADDR_FASTCOM_SERIAL    : natural :=  7;
 constant IPBADDR_FASTCOM_CONTROL   : natural :=  8; 
 
 --constant IPBADDR_CHIPID            : natural :=  4;
-- constant IPBADDR_RO_CONFIG_1       : natural :=  5;
-- constant IPBADDR_RO_CONFIG_2       : natural :=  6;
-- constant IPBADDR_RO_CONFIG_3       : natural :=  7;
-- constant IPBADDR_RO_FAST_READ      : natural :=  8;
-- constant IPBADDR_RO_SLOW_READ      : natural :=  9;
-- constant IPBADDR_RO_INFO           : natural := 10;
-- constant IPBADDR_RO_DEBUG_READ     : natural := 11;
-- constant IPBADDR_L1A_COUNTER       : natural := 12;
-- constant IPBADDR_RO_SLOWFIFO_INFO  : natural := 13;
 
 -- SC LARGE WORDS T0/FROM FIFO

 --constant IPBADDR_RWORD                  : natural := 84;
 
---- SC SMALL WORDS
-- constant IPBADDR_SC_SMALL_1_W                  : natural :=  14;
-- constant IPBADDR_LVDS_BRIDGE_CMF_IB_IV         : natural :=  15; 
-- constant IPBADDR_SWCNTL_IBUFMON                : natural :=  16;
-- constant IPBADDR_PULSE_MON_DELAY               : natural :=  17; 
 
-- constant IPBADDR_SC_SMALL_1_R                  : natural :=  18;
-- constant IPBADDR_LVDS_BRIDGE_CMF_IB_IV_R       : natural :=  19; 
-- constant IPBADDR_SWCNTL_IBUFMON_R              : natural :=  20;
-- constant IPBADDR_PULSE_MON_DELAY_R             : natural :=  21; 
 
 
 constant SLOWDATA_PACKET_HEADER_LENGTH         : natural := 10;
end;
