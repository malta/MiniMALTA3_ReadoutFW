-- The ipbus bus fabric, address select logic, data multiplexers
--
-- The address table is encoded in ipbus_addr_decode package - no need to change
-- anything in this file.
--
-- Dave Newbold, February 2011

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use work.ipbus.ALL;

entity ipbus_fabric is
  generic(
    NSLV: positive;
    STROBE_GAP: boolean := false
   );
  port(
    ipb_in: in ipb_wbus;
    ipb_out: out ipb_rbus;
    ipb_to_slaves: out ipb_wbus_array(NSLV - 1 downto 0);
    ipb_from_slaves: in ipb_rbus_array(NSLV - 1 downto 0)
   );

end ipbus_fabric;

architecture rtl of ipbus_fabric is

	signal sel: integer := 0;
	signal ored_ack, ored_err: std_logic_vector(NSLV downto 0);
	signal qstrobe: std_logic;
    signal sel_array: std_logic_vector(99 downto 0);
    
begin

    --carlos's sel
    selector: for i in NSLV-1 downto 0 generate
    begin
        sel_array(i) <= '1' when to_integer(unsigned(ipb_in.ipb_addr)) = to_integer(unsigned(ipb_from_slaves(i).ipb_addr)) else '0';
    end generate;
    
	process(ipb_in.ipb_addr)
	begin
        --default sel
		--sel <= ipbus_addr_sel(ipb_in.ipb_addr);
        --carlos's sel
        if    sel_array(0)='1' then sel <= 0;
        elsif sel_array(1)='1' then sel <= 1;
        elsif sel_array(2)='1' then sel <= 2;
        elsif sel_array(3)='1' then sel <= 3;
        elsif sel_array(4)='1' then sel <= 4;
        elsif sel_array(5)='1' then sel <= 5;
        elsif sel_array(6)='1' then sel <= 6;
        elsif sel_array(7)='1' then sel <= 7;
        elsif sel_array(8)='1' then sel <= 8;
        elsif sel_array(9)='1' then sel <= 9;
        elsif sel_array(10)='1' then sel <= 10;
        elsif sel_array(11)='1' then sel <= 11;
        elsif sel_array(12)='1' then sel <= 12;
        elsif sel_array(13)='1' then sel <= 13;
        elsif sel_array(14)='1' then sel <= 14;
        elsif sel_array(15)='1' then sel <= 15;
        elsif sel_array(16)='1' then sel <= 16;
        elsif sel_array(17)='1' then sel <= 17;
        elsif sel_array(18)='1' then sel <= 18;
        elsif sel_array(19)='1' then sel <= 19;
        elsif sel_array(20)='1' then sel <= 20;
        elsif sel_array(21)='1' then sel <= 21;
        elsif sel_array(22)='1' then sel <= 22;
        elsif sel_array(23)='1' then sel <= 23;
        elsif sel_array(24)='1' then sel <= 24;
        elsif sel_array(25)='1' then sel <= 25;
        elsif sel_array(26)='1' then sel <= 26;
        elsif sel_array(27)='1' then sel <= 27;
        elsif sel_array(28)='1' then sel <= 28;
        elsif sel_array(29)='1' then sel <= 29;
        elsif sel_array(30)='1' then sel <= 30;
        elsif sel_array(31)='1' then sel <= 31;
        elsif sel_array(32)='1' then sel <= 32;
        elsif sel_array(33)='1' then sel <= 33;
        elsif sel_array(34)='1' then sel <= 34;
        elsif sel_array(35)='1' then sel <= 35;
        elsif sel_array(36)='1' then sel <= 36;
        elsif sel_array(37)='1' then sel <= 37;
        elsif sel_array(38)='1' then sel <= 38;
        elsif sel_array(39)='1' then sel <= 39;
        elsif sel_array(40)='1' then sel <= 40;
        elsif sel_array(41)='1' then sel <= 41;
        elsif sel_array(42)='1' then sel <= 42;
        elsif sel_array(43)='1' then sel <= 43;
        elsif sel_array(44)='1' then sel <= 44;
        elsif sel_array(45)='1' then sel <= 45;
        elsif sel_array(46)='1' then sel <= 46;
        elsif sel_array(47)='1' then sel <= 47;
        elsif sel_array(48)='1' then sel <= 48;
        elsif sel_array(49)='1' then sel <= 49;
        elsif sel_array(50)='1' then sel <= 50;
        elsif sel_array(51)='1' then sel <= 51;
        elsif sel_array(52)='1' then sel <= 52;
        elsif sel_array(53)='1' then sel <= 53;
        elsif sel_array(54)='1' then sel <= 54;
        elsif sel_array(55)='1' then sel <= 55;
        elsif sel_array(56)='1' then sel <= 56;
        elsif sel_array(57)='1' then sel <= 57;
        elsif sel_array(58)='1' then sel <= 58;
        elsif sel_array(59)='1' then sel <= 59;
        elsif sel_array(60)='1' then sel <= 60;
        elsif sel_array(61)='1' then sel <= 61;
        elsif sel_array(62)='1' then sel <= 62;
        elsif sel_array(63)='1' then sel <= 63;
        elsif sel_array(64)='1' then sel <= 64;
        elsif sel_array(65)='1' then sel <= 65;
        elsif sel_array(66)='1' then sel <= 66;
        elsif sel_array(67)='1' then sel <= 67;
        elsif sel_array(68)='1' then sel <= 68;
        elsif sel_array(69)='1' then sel <= 69;
        elsif sel_array(70)='1' then sel <= 70;
        elsif sel_array(71)='1' then sel <= 71;
        elsif sel_array(72)='1' then sel <= 72;
        elsif sel_array(73)='1' then sel <= 73;
        elsif sel_array(74)='1' then sel <= 74;
        elsif sel_array(75)='1' then sel <= 75;
        elsif sel_array(76)='1' then sel <= 76;
        elsif sel_array(77)='1' then sel <= 77;
        elsif sel_array(78)='1' then sel <= 78;
        elsif sel_array(79)='1' then sel <= 79;
        elsif sel_array(80)='1' then sel <= 80;
        elsif sel_array(81)='1' then sel <= 81;
        elsif sel_array(82)='1' then sel <= 82;
        elsif sel_array(83)='1' then sel <= 83;
        elsif sel_array(84)='1' then sel <= 84;
        elsif sel_array(85)='1' then sel <= 85;
        elsif sel_array(86)='1' then sel <= 86;
        elsif sel_array(87)='1' then sel <= 87;
        elsif sel_array(88)='1' then sel <= 88;
        elsif sel_array(89)='1' then sel <= 89;
        elsif sel_array(90)='1' then sel <= 90;
        elsif sel_array(91)='1' then sel <= 91;
        elsif sel_array(92)='1' then sel <= 92;
        elsif sel_array(93)='1' then sel <= 93;
        elsif sel_array(94)='1' then sel <= 94;
        elsif sel_array(95)='1' then sel <= 95;
        elsif sel_array(96)='1' then sel <= 96;
        elsif sel_array(97)='1' then sel <= 97;
        elsif sel_array(98)='1' then sel <= 98;
        else 
            sel <= 99;
        end if;
    end process;

	ored_ack(NSLV) <= '0';
	ored_err(NSLV) <= '0';
	
	qstrobe <= ipb_in.ipb_strobe when STROBE_GAP = false else
	 ipb_in.ipb_strobe and not (ored_ack(0) or ored_err(0));

	busgen: for i in NSLV-1 downto 0 generate
	begin

		ipb_to_slaves(i).ipb_addr <= ipb_in.ipb_addr;
		ipb_to_slaves(i).ipb_wdata <= ipb_in.ipb_wdata;
		ipb_to_slaves(i).ipb_strobe <= qstrobe when sel=i else '0';
		ipb_to_slaves(i).ipb_write <= ipb_in.ipb_write;
		ored_ack(i) <= ored_ack(i+1) or ipb_from_slaves(i).ipb_ack;
		ored_err(i) <= ored_err(i+1) or ipb_from_slaves(i).ipb_err;		

	end generate;

  ipb_out.ipb_rdata <= ipb_from_slaves(sel).ipb_rdata when sel /= 99 else (others => '0');
  ipb_out.ipb_ack <= ored_ack(0);
  ipb_out.ipb_err <= ored_err(0);
  ipb_out.ipb_addr <= (others => '0');
  
end rtl;

