library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

package package_constants is
    constant NBITS          : natural := 16; -- Slow Control word size
    constant NBITSIPBUS     : natural := 32; -- IPbus word size
    constant NCLOCKSLOWDOWN : natural := 10000000;
    constant DUMMYWORD      : std_logic_vector(NBITS-1 downto 0) := B"0011000111000011";
    constant NULLWORD       : std_logic_vector(NBITS-1 downto 0) := B"0000000000000000";    
    constant FULLWORD       : std_logic_vector(NBITS-1 downto 0) := B"1111111111111111";
    constant FIFOINSIZE     : natural := 2;
    constant FIFOOUTSIZE    : natural := 4;
    constant CLKPERIOD      : time := 100 ns; -- for 10MHz, use 50ns
    --constant SCNBITS        : natural := 4321;
    constant CC_WRITE_FIFO  : natural := 18;
end;