#create_clock -period  1.5625 -name CLK_640 -waveform {0.000  0.7812} [get_pins clk1/clk_640MHz]
#create_clock -period  1.6667 -name CLK_600 -waveform {0.000  0.8333} [get_pins clk2/clk_600MHz]
#create_clock -period  5.0000 -name CLK_200 -waveform {0.000  2.5000} [get_pins clk2/clk_200MHz]
#create_clock -period  6.6667 -name CLK_150 -waveform {0.000  3.3333} [get_pins clk2/clk_150MHz]
#create_clock -period 25.0000 -name CLK_40  -waveform {0.000 12.5000} [get_pins clk2/clk_40MHz]

#create_clock -period  1.6667 -name CLK_OS_000_600 - waveform {0.000 0.8333} [get_pins Clock/out_clk_000_600]
#create_clock -period  1.6667 -name CLK_OS_090_600 - waveform {0.000 0.8333} [get_pins Clock/out_clk_090_600]
#create_clock -period  1.6667 -name CLK_OS_180_600 - waveform {0.000 0.8333} [get_pins Clock/out_clk_180_600]
#create_clock -period  1.6667 -name CLK_OS_270_600 - waveform {0.000 0.8333} [get_pins Clock/out_clk_270_600]
#create_clock -period  2.5    -name CLK_OS_000_400 - waveform {0.000 1.25  } [get_pins Clock/out_clk_000_400]
#create_clock -period  6.6667 -name CLK_OS_000_150 - waveform {0.000 3.3333} [get_pins Clock/out_clk_000_150]

#####################################################################################################################################
# write constraints for
# i_FDATA_IN_P/N -> IBUFDS -> IDELAY -> ISERDESE2 -> s_sampled_bits


#create_clock -period  5.000 -name CLK_200_IPB -waveform {0.000  2.500} [get_pins clk2/clk_200MHz]
#create_clock -period 33.333 -name CLK_SC      -waveform {0.000 16.666} [get_pins clk2/clk_30MHz]
#create_clock -period 25.000 -name CLK_SLOW    -waveform {0.000 12.500} [get_pins clk1/clk_40MHz]

#create_clock -period  3.125 -name CLK_FAST    -waveform {0.000  1.5625} [get_pins clk1/clk_640MHz]

##false paths
set_false_path -from [get_clocks -regexp .*] -to [get_ports {{o_TOP_LEDS[0]} {o_TOP_LEDS[1]} {o_TOP_LEDS[2]} {o_TOP_LEDS[3]} {o_TOP_LEDS[4]} {o_TOP_LEDS[5]} {o_TOP_LEDS[6]} {o_TOP_LEDS[7]}}]
set_false_path -from [get_clocks -regexp .*] -to [get_ports {{o_LPC_LEDS[0]} {o_LPC_LEDS[1]} {o_LPC_LEDS[2]} {o_LPC_LEDS[3]} {o_LPC_LEDS[4]} {o_LPC_LEDS[5]} {o_LPC_LEDS[6]} {o_LPC_LEDS[7]} {o_LPC_LEDS[8]} {o_LPC_LEDS[9]}}]
set_false_path -from [get_clocks -regexp .*] -to [get_ports o_SMA_P]
set_false_path -from [get_clocks -regexp .*] -to [get_ports o_SMA_N]
set_false_path -from [get_clocks -regexp .*] -to [get_ports o_rstB_TOMALTA]
set_false_path -from [get_clocks -regexp .*] -to [get_ports o_VPULSE]
set_false_path -from [get_clocks -regexp .*] -to [get_ports o_SLOW_DATA]

set_false_path -from [get_ports {i_PUSH_0 i_PUSH_1 i_PUSH_2 i_PUSH_3 i_PUSH_4}] -to [get_clocks -regexp .*]
set_false_path -from [get_ports {{i_SWI[0]} {i_SWI[1]} {i_SWI[2]} {i_SWI[3]}}] -to [get_clocks -regexp .*]


create_generated_clock -name CLK_SLOW -source [get_pins clk1/inst/mmcm_adv_inst/CLKIN1] -master_clock clk1/inst/clk_in1 [get_pins clk1/inst/mmcm_adv_inst/CLKOUT2]
create_generated_clock -name CLK_SC -source [get_pins clk2/inst/mmcm_adv_inst/CLKIN1] -master_clock clk2/inst/clk_in1 [get_pins clk2/inst/mmcm_adv_inst/CLKOUT1]
create_generated_clock -name CLK_FAST -source [get_pins clk1/inst/mmcm_adv_inst/CLKIN1] -master_clock clk1/inst/clk_in1 [get_pins clk1/inst/mmcm_adv_inst/CLKOUT0]
create_generated_clock -name CLK_FAST2 -source [get_pins clk1/inst/mmcm_adv_inst/CLKIN1] -master_clock clk1/inst/clk_in1 [get_pins clk1/inst/mmcm_adv_inst/CLKOUT1]

##clk decouplings
set_false_path -from [get_clocks [get_clocks -of_objects [get_pins ipbus/clocks/mmcm/CLKOUT1]]] -to [get_clocks CLK_SC]
set_false_path -from [get_clocks CLK_SC] -to [get_clocks [get_clocks -of_objects [get_pins ipbus/clocks/mmcm/CLKOUT1]]]

set_false_path -from [get_clocks [get_clocks -of_objects [get_pins ipbus/clocks/mmcm/CLKOUT1]]] -to [get_clocks CLK_SLOW]
set_false_path -from [get_clocks CLK_SLOW] -to [get_clocks [get_clocks -of_objects [get_pins ipbus/clocks/mmcm/CLKOUT1]]]

set_false_path -from [get_clocks [get_clocks -of_objects [get_pins ipbus/clocks/mmcm/CLKOUT1]]] -to [get_clocks CLK_FAST]
set_false_path -from [get_clocks CLK_FAST] -to [get_clocks [get_clocks -of_objects [get_pins ipbus/clocks/mmcm/CLKOUT1]]]

set_false_path -from [get_clocks [get_clocks -of_objects [get_pins ipbus/clocks/mmcm/CLKOUT1]]] -to [get_clocks CLK_FAST2]
set_false_path -from [get_clocks CLK_FAST2] -to [get_clocks [get_clocks -of_objects [get_pins ipbus/clocks/mmcm/CLKOUT1]]]

set_input_delay 0.000 [get_ports {i_FDOUT_N i_FDOUT_P}]
