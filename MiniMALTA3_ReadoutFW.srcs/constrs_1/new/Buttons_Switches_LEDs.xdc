
#4 Pole Dip Switches
set_property PACKAGE_PIN Y29 [get_ports {i_SWI[0]}]
###Used for IP address
set_property PACKAGE_PIN W29 [get_ports {i_SWI[1]}]
###Used for IP address
set_property PACKAGE_PIN AA28 [get_ports {i_SWI[2]}]
###Used for IP address
set_property PACKAGE_PIN Y28 [get_ports {i_SWI[3]}]
###Used for new DHCP flag in IPbus
set_property IOSTANDARD LVCMOS25 [get_ports {i_SWI[0]}]
set_property IOSTANDARD LVCMOS25 [get_ports {i_SWI[1]}]
set_property IOSTANDARD LVCMOS25 [get_ports {i_SWI[2]}]
set_property IOSTANDARD LVCMOS25 [get_ports {i_SWI[3]}]


# GPIO leds
set_property PACKAGE_PIN AB8 [get_ports {o_TOP_LEDS[0]}]
set_property IOSTANDARD LVCMOS15 [get_ports {o_TOP_LEDS[0]}]
set_property PACKAGE_PIN AA8 [get_ports {o_TOP_LEDS[1]}]
set_property IOSTANDARD LVCMOS15 [get_ports {o_TOP_LEDS[1]}]
set_property PACKAGE_PIN AC9 [get_ports {o_TOP_LEDS[2]}]
set_property IOSTANDARD LVCMOS15 [get_ports {o_TOP_LEDS[2]}]
set_property PACKAGE_PIN AB9 [get_ports {o_TOP_LEDS[3]}]
set_property IOSTANDARD LVCMOS15 [get_ports {o_TOP_LEDS[3]}]
set_property PACKAGE_PIN AE26 [get_ports {o_TOP_LEDS[4]}]
set_property IOSTANDARD LVCMOS25 [get_ports {o_TOP_LEDS[4]}]
set_property PACKAGE_PIN G19 [get_ports {o_TOP_LEDS[5]}]
set_property IOSTANDARD LVCMOS25 [get_ports {o_TOP_LEDS[5]}]
set_property PACKAGE_PIN E18 [get_ports {o_TOP_LEDS[6]}]
set_property IOSTANDARD LVCMOS25 [get_ports {o_TOP_LEDS[6]}]
set_property PACKAGE_PIN F16 [get_ports {o_TOP_LEDS[7]}]
set_property IOSTANDARD LVCMOS25 [get_ports {o_TOP_LEDS[7]}]

#HPC Ext. Board Leds
#set_property PACKAGE_PIN H22 [get_ports {o_HPC_LEDS[0]}]
#set_property IOSTANDARD LVCMOS25 [get_ports {o_HPC_LEDS[0]}]
#set_property PACKAGE_PIN H21 [get_ports {o_HPC_LEDS[1]}]
#set_property IOSTANDARD LVCMOS25 [get_ports {o_HPC_LEDS[1]}]
#set_property PACKAGE_PIN F22 [get_ports {o_HPC_LEDS[2]}]
#set_property IOSTANDARD LVCMOS25 [get_ports {o_HPC_LEDS[2]}]
#set_property PACKAGE_PIN G22 [get_ports {o_HPC_LEDS[3]}]
#set_property IOSTANDARD LVCMOS25 [get_ports {o_HPC_LEDS[3]}]
#set_property PACKAGE_PIN B17 [get_ports {o_HPC_LEDS[4]}]
#set_property IOSTANDARD LVCMOS25 [get_ports {o_HPC_LEDS[4]}]
#set_property PACKAGE_PIN C17 [get_ports {o_HPC_LEDS[5]}]
#set_property IOSTANDARD LVCMOS25 [get_ports {o_HPC_LEDS[5]}]
#set_property PACKAGE_PIN F17 [get_ports {o_HPC_LEDS[6]}]
#set_property IOSTANDARD LVCMOS25 [get_ports {o_HPC_LEDS[6]}]
#set_property PACKAGE_PIN G17 [get_ports {o_HPC_LEDS[7]}]
#set_property IOSTANDARD LVCMOS25 [get_ports {o_HPC_LEDS[7]}]
#set_property PACKAGE_PIN B20 [get_ports {o_HPC_LEDS[8]}]
#set_property IOSTANDARD LVCMOS25 [get_ports {o_HPC_LEDS[8]}]
#set_property PACKAGE_PIN C20 [get_ports {o_HPC_LEDS[9]}]
#set_property IOSTANDARD LVCMOS25 [get_ports {o_HPC_LEDS[9]}]

## push button
set_property PACKAGE_PIN AG5 [get_ports i_PUSH_0]
set_property PACKAGE_PIN AC6 [get_ports i_PUSH_1]
set_property PACKAGE_PIN AB12 [get_ports i_PUSH_2]
set_property PACKAGE_PIN AA12 [get_ports i_PUSH_3]
set_property PACKAGE_PIN G12 [get_ports i_PUSH_4]
set_property IOSTANDARD LVCMOS15 [get_ports i_PUSH_0]
set_property IOSTANDARD LVCMOS15 [get_ports i_PUSH_1]
set_property IOSTANDARD LVCMOS15 [get_ports i_PUSH_2]
set_property IOSTANDARD LVCMOS15 [get_ports i_PUSH_3]
set_property IOSTANDARD LVCMOS25 [get_ports i_PUSH_4]

## FMC
#set_property PACKAGE_PIN H22     [get_ports O_CLK_FAST_N]
#set_property IOSTANDARD  LVDS_25 [get_ports O_CLK_FAST_N]
#set_property PACKAGE_PIN H21     [get_ports O_CLK_FAST_P]
#set_property IOSTANDARD  LVDS_25 [get_ports O_CLK_FAST_P]
#set_property PACKAGE_PIN F22     [get_ports O_CLK_DIVIDED_N]
#set_property IOSTANDARD  LVDS_25 [get_ports O_CLK_DIVIDED_N]
#set_property PACKAGE_PIN G22     [get_ports O_CLK_DIVIDED_P]
#set_property IOSTANDARD  LVDS_25 [get_ports O_CLK_DIVIDED_P]
#set_property PACKAGE_PIN B17     [get_ports I_SERIALSTREAM_N]
#set_property IOSTANDARD  LVDS_25 [get_ports I_SERIALSTREAM_N]
#set_property PACKAGE_PIN C17     [get_ports I_SERIALSTREAM_P]
#set_property IOSTANDARD  LVDS_25 [get_ports I_SERIALSTREAM_P]
#set_property PACKAGE_PIN F17     [get_ports O_TESTLINE_N]
#set_property IOSTANDARD  LVDS_25 [get_ports O_TESTLINE_N]
#set_property PACKAGE_PIN G17     [get_ports O_TESTLINE_P]
#set_property IOSTANDARD  LVDS_25 [get_ports O_TESTLINE_P]

# SMA
set_property PACKAGE_PIN Y23 [get_ports o_SMA_P]
set_property IOSTANDARD LVCMOS25 [get_ports o_SMA_P]
set_property PACKAGE_PIN Y24 [get_ports i_SMA_N]
set_property IOSTANDARD LVCMOS25 [get_ports i_SMA_N]



