set_property PACKAGE_PIN A16 [get_ports o_SC_DATA]
set_property IOSTANDARD LVCMOS25 [get_ports o_SC_DATA]

set_property PACKAGE_PIN E28 [get_ports i_SOUT]
set_property IOSTANDARD LVCMOS25 [get_ports i_SOUT]

set_property PACKAGE_PIN C17 [get_ports o_SC_CLK]
#set_property PACKAGE_PIN G29 [get_ports o_SC_CLK]
set_property IOSTANDARD LVCMOS25 [get_ports o_SC_CLK]

set_property PACKAGE_PIN D22 [get_ports o_SC_LOAD]
set_property IOSTANDARD LVCMOS25 [get_ports o_SC_LOAD ]

set_property PACKAGE_PIN D16 [get_ports o_rstB_TOMALTA] 
set_property IOSTANDARD LVCMOS25 [get_ports o_rstB_TOMALTA]


set_property PACKAGE_PIN G27 [get_ports o_REFPULSE_Valid] 
set_property IOSTANDARD LVCMOS25 [get_ports o_REFPULSE_Valid]


set_property PACKAGE_PIN G17 [get_ports o_SRVpulse] 
set_property IOSTANDARD LVCMOS25 [get_ports o_SRVpulse]


############################## FAST COMMANDER ##################################
#set_property PACKAGE_PIN H21 [get_ports o_CLK_160_P]
#set_property IOSTANDARD LVCMOS25 [get_ports o_CLK_160_P]


############################## BACKUP CLOCK PLL ##################################


set_property PACKAGE_PIN F21 [get_ports o_PLL_BYPASS_CLK_P]
set_property PACKAGE_PIN E21 [get_ports o_PLL_BYPASS_CLK_N]
set_property DIFF_TERM TRUE [get_ports o_PLL_BYPASS_CLK_P]
set_property DIFF_TERM TRUE [get_ports o_PLL_BYPASS_CLK_N]
set_property IOSTANDARD LVDS_25 [get_ports o_PLL_BYPASS_CLK_P]
set_property IOSTANDARD LVDS_25 [get_ports o_PLL_BYPASS_CLK_N]


set_property PACKAGE_PIN H30 [get_ports o_PLL_SOURCE_CLK_P]
set_property PACKAGE_PIN G30 [get_ports o_PLL_SOURCE_CLK_N]
set_property DIFF_TERM TRUE [get_ports o_PLL_SOURCE_CLK_P]
set_property DIFF_TERM TRUE [get_ports o_PLL_SOURCE_CLK_N]
set_property IOSTANDARD LVDS_25 [get_ports o_PLL_SOURCE_CLK_P]
set_property IOSTANDARD LVDS_25 [get_ports o_PLL_SOURCE_CLK_N]


set_property PACKAGE_PIN H26 [get_ports o_CHIPID[0]]
set_property PACKAGE_PIN E29 [get_ports o_CHIPID[1]]
set_property PACKAGE_PIN C29 [get_ports o_CHIPID[2]]
set_property IOSTANDARD LVCMOS25 [get_ports o_CHIPID[0]]
set_property IOSTANDARD LVCMOS25 [get_ports o_CHIPID[1]]
set_property IOSTANDARD LVCMOS25 [get_ports o_CHIPID[2]]

set_property PACKAGE_PIN B18 [get_ports o_SLOW_DATA]
set_property IOSTANDARD LVCMOS25 [get_ports o_SLOW_DATA]

#set_property PACKAGE_PIN A17 [get_ports o_VREF]
#set_property IOSTANDARD LVCMOS25 [get_ports o_VREF]

#set_property PACKAGE_PIN C16 [get_ports o_VREF1]
#set_property IOSTANDARD LVCMOS25 [get_ports o_VREF1]

###############################DEBUG_DATA_PATH##############################
set_property PACKAGE_PIN A25 [get_ports i_DEBUG_DATA_PATH]
set_property IOSTANDARD LVCMOS25 [get_ports i_DEBUG_DATA_PATH]

###############################FASTCOM MODULE##############################
set_property PACKAGE_PIN H21 [get_ports o_FASTCOM_SERIAL]
set_property IOSTANDARD LVCMOS25 [get_ports o_FASTCOM_SERIAL]

set_property PACKAGE_PIN G22 [get_ports o_FASTCOM_RESET]
set_property IOSTANDARD LVCMOS25 [get_ports o_FASTCOM_RESET]