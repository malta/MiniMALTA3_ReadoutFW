#LPC Ext. Board Leds
set_property PACKAGE_PIN AC30 [get_ports {o_LPC_LEDS[0]}]
set_property IOSTANDARD LVCMOS25 [get_ports {o_LPC_LEDS[0]}]
set_property PACKAGE_PIN AC29 [get_ports {o_LPC_LEDS[1]}]
set_property IOSTANDARD LVCMOS25 [get_ports {o_LPC_LEDS[1]}]
set_property PACKAGE_PIN AE29 [get_ports {o_LPC_LEDS[2]}]
set_property IOSTANDARD LVCMOS25 [get_ports {o_LPC_LEDS[2]}]
set_property PACKAGE_PIN AD29 [get_ports {o_LPC_LEDS[3]}]
set_property IOSTANDARD LVCMOS25 [get_ports {o_LPC_LEDS[3]}]
set_property PACKAGE_PIN AF28 [get_ports {o_LPC_LEDS[4]}]
set_property IOSTANDARD LVCMOS25 [get_ports {o_LPC_LEDS[4]}]
set_property PACKAGE_PIN AE28 [get_ports {o_LPC_LEDS[5]}]
set_property IOSTANDARD LVCMOS25 [get_ports {o_LPC_LEDS[5]}]
set_property PACKAGE_PIN AD26 [get_ports {o_LPC_LEDS[6]}]
set_property IOSTANDARD LVCMOS25 [get_ports {o_LPC_LEDS[6]}]
set_property PACKAGE_PIN AC26 [get_ports {o_LPC_LEDS[7]}]
set_property IOSTANDARD LVCMOS25 [get_ports {o_LPC_LEDS[7]}]
set_property PACKAGE_PIN AK28 [get_ports {o_LPC_LEDS[8]}]
set_property IOSTANDARD LVCMOS25 [get_ports {o_LPC_LEDS[8]}]
set_property PACKAGE_PIN AJ27 [get_ports {o_LPC_LEDS[9]}]
set_property IOSTANDARD LVCMOS25 [get_ports {o_LPC_LEDS[9]}]

#LPC Ext. Board SMA
#LA02 P
set_property PACKAGE_PIN AF20 [get_ports {o_LPC_SMA[0]}]
set_property IOSTANDARD LVCMOS25 [get_ports {o_LPC_SMA[0]}]
#LA02 N
set_property PACKAGE_PIN AF21 [get_ports {o_LPC_SMA[1]}]
set_property IOSTANDARD LVCMOS25 [get_ports {o_LPC_SMA[1]}]
#LA18_CC PAnd 
set_property PACKAGE_PIN AD27 [get_ports {o_LPC_SMA[2]}]
set_property IOSTANDARD LVCMOS25 [get_ports {o_LPC_SMA[2]}]
#LA18_CC N
set_property PACKAGE_PIN AD28 [get_ports {o_LPC_SMA[3]}]
set_property IOSTANDARD LVCMOS25 [get_ports {o_LPC_SMA[3]}]
#LA00 P
set_property PACKAGE_PIN AD23 [get_ports {o_LPC_SMA[4]}]
set_property IOSTANDARD LVCMOS25 [get_ports {o_LPC_SMA[4]}]
#LA00 N
set_property PACKAGE_PIN AE24 [get_ports {o_LPC_SMA[5]}]
set_property IOSTANDARD LVCMOS25 [get_ports {o_LPC_SMA[5]}]
#LA06 P
set_property PACKAGE_PIN AK20 [get_ports {o_LPC_SMA[6]}]
set_property IOSTANDARD LVCMOS25 [get_ports {o_LPC_SMA[6]}]
#LA06 N
set_property PACKAGE_PIN AK21 [get_ports {o_LPC_SMA[7]}]
set_property IOSTANDARD LVCMOS25 [get_ports {o_LPC_SMA[7]}]
##LA02 P
#set_property PACKAGE_PIN N8 [get_ports {o_LPC_SMA[8]}]
#set_property IOSTANDARD LVCMOS25 [get_ports {o_LPC_SMA[8]}]
##LA02 N
#set_property PACKAGE_PIN N7 [get_ports {o_LPC_SMA[9]}]
#set_property IOSTANDARD LVCMOS25 [get_ports {o_LPC_SMA[9]}]
#LA01_CC P
set_property PACKAGE_PIN AE23 [get_ports {o_LPC_SMA[8]}]
set_property IOSTANDARD LVCMOS25 [get_ports {o_LPC_SMA[8]}]
#LA01_CC N
set_property PACKAGE_PIN AF23 [get_ports {o_LPC_SMA[9]}]
set_property IOSTANDARD LVCMOS25 [get_ports {o_LPC_SMA[9]}]








