set_property PACKAGE_PIN C19 [get_ports i_rst_SCEM]   
set_property IOSTANDARD LVCMOS25 [get_ports i_rst_SCEM]

set_property PACKAGE_PIN H30 [get_ports i_data_SCEM]
set_property IOSTANDARD LVCMOS25 [get_ports _data_SCEM]

set_property PACKAGE_PIN B22 [get_ports o_sout_SCEM]
set_property IOSTANDARD LVCMOS25 [get_ports o_sout_SCEM]

set_property PACKAGE_PIN D29 [get_ports i_clk_SCEM]
set_property IOSTANDARD LVCMOS25 [get_ports i_clk_SCEM]

set_property PACKAGE_PIN B19 [get_ports i_load_SCEM]
set_property IOSTANDARD LVCMOS25 [get_ports i_load_SCEM]

