### clocks for fast and slow readout #######################################################################################################
set_property PACKAGE_PIN D26 [get_ports o_CLK_640_P]
set_property PACKAGE_PIN C26 [get_ports o_CLK_640_N]
set_property PACKAGE_PIN H24 [get_ports o_CLK_40_P]
set_property PACKAGE_PIN H25 [get_ports o_CLK_40_N]

set_property DIFF_TERM TRUE [get_ports o_CLK_640_P]
set_property DIFF_TERM TRUE [get_ports o_CLK_640_N]
set_property DIFF_TERM TRUE [get_ports o_CLK_40_P]
set_property DIFF_TERM TRUE [get_ports o_CLK_40_N]

#Commented by Lluis
set_property IOSTANDARD LVDS_25 [get_ports o_CLK_640_P]
set_property IOSTANDARD LVDS_25 [get_ports o_CLK_640_N]
set_property IOSTANDARD LVDS_25 [get_ports o_CLK_40_P]
set_property IOSTANDARD LVDS_25 [get_ports o_CLK_40_N]

### data for fast and slow readout #######################################################################################################
#set_property PACKAGE_PIN G29 [get_ports i_FDOUT_P]
set_property PACKAGE_PIN F30 [get_ports i_FDOUT_N]
set_property PACKAGE_PIN A25 [get_ports i_SDOUT_P]
set_property PACKAGE_PIN A26 [get_ports i_SDOUT_N]
set_property PACKAGE_PIN B30 [get_ports i_SDOUT_ACK_P]
set_property PACKAGE_PIN A30 [get_ports i_SDOUT_ACK_N]

set_property DIFF_TERM TRUE [get_ports i_FDOUT_P]
set_property DIFF_TERM TRUE [get_ports i_FDOUT_N]
set_property DIFF_TERM TRUE [get_ports i_SDOUT_P]
set_property DIFF_TERM TRUE [get_ports i_SDOUT_N]
set_property DIFF_TERM TRUE [get_ports i_SDOUT_ACK_P]
set_property DIFF_TERM TRUE [get_ports i_SDOUT_ACK_N]

#set_property IOSTANDARD LVCMOS25 [get_ports io_DATA_SELECT]
set_property IOSTANDARD LVDS_25 [get_ports i_FDOUT_P]
set_property IOSTANDARD LVDS_25 [get_ports i_FDOUT_N]
set_property IOSTANDARD LVDS_25 [get_ports i_SDOUT_P]
set_property IOSTANDARD LVDS_25 [get_ports i_SDOUT_N]
set_property IOSTANDARD LVDS_25 [get_ports i_SDOUT_ACK_P]
set_property IOSTANDARD LVDS_25 [get_ports i_SDOUT_ACK_N]



