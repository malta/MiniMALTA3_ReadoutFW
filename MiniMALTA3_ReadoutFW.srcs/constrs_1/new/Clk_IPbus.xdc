create_clock -period 8.000 -name SGMIICLK_Q0_P -waveform {0.000 4.000} [get_ports SGMIICLK_Q0_P]

# Local oscillator clock in KC705
set_property PACKAGE_PIN AD11 [get_ports i_sys_clk_N]
set_property PACKAGE_PIN AD12 [get_ports i_sys_clk_P]
set_property IOSTANDARD DIFF_SSTL15 [get_ports i_sys_clk_P]
set_property IOSTANDARD DIFF_SSTL15 [get_ports i_sys_clk_N]
set_property VCCAUX_IO DONTCARE [get_ports i_sys_clk_P]

#######################################################################################################################################################
#######################################################################################################################################################

#Ethernet PHY pin K3
set_property PACKAGE_PIN L20 [get_ports PHY_RESET]
set_property IOSTANDARD LVCMOS25 [get_ports PHY_RESET]
#Ethernet PHY pin A3
#Ethernet PHY pin A4
#Ethernet PHY pin A7
#Ethernet PHY pin A8
set_property PACKAGE_PIN H5 [get_ports SGMII_RX_N]
set_property PACKAGE_PIN H6 [get_ports SGMII_RX_P]
set_property PACKAGE_PIN J3 [get_ports SGMII_TX_N]
set_property PACKAGE_PIN J4 [get_ports SGMII_TX_P]
#Ethernet PHY pin U2_6
#Ethernet PHY pin U2_7
set_property PACKAGE_PIN G8 [get_ports SGMIICLK_Q0_P]
set_property PACKAGE_PIN G7 [get_ports SGMIICLK_Q0_N]






